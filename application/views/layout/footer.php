<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2017-2019 <a href="#">RANEF</a>.</strong> All rights
    reserved.
  </footer>