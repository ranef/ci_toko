<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
$user_login=$this->session->userdata();
?>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>Master</title>
<link rel="stylesheet" href="<?php echo base_url()?>/assets/css/style.css"/> 
<link href="<?php echo base_url(); ?>/assets/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url()?>/assets/date_picker_bootstrap/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
</head>
<body>
<table width="100%" border="0" align="center" name="isi">
  <tr>
    <td width="100">
<table >
    <tr align="center">
        <td height="10">
          <?php 
				if ($user_login['tipe']=="1"){
					$this->load->view('template/view_menu');} else {
						$this->load->view('template/view_menu_user');}
			?>
		</td>
    </tr>
</table>
<tr height="50px">
        <td>&nbsp;
       </td>
      </tr>
      <tr>
        <td>
       		<?php $this->load->view($content);?>
       </td>
      </tr>
    </table></td>
  </tr>
</table>
<!-- jQuery Version 1.11.0 -->
<script src="<?php echo base_url() ?>/assets/jquery-1.11.0.js"></script>
<script src="<?php echo base_url(); ?>/assets/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>/assets/date_picker_bootstrap/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript" src="<?php echo base_url()?>/assets/date_picker_bootstrap/js/locales/bootstrap-datetimepicker.id.js"charset="UTF-8"></script>
 <script type="text/javascript">
  $('.datepicker').datetimepicker({
        language:  'id',
        weekStart: 1,
        todayBtn:  1,
  autoclose: 1,
  todayHighlight: 1,
  startView: 2,
  minView: 2,
  forceParse: 0,
  dateFormat:"YY-mm-dd"
    });
    $('.datepicker1').datetimepicker({
        language:  'id',
        weekStart: 1,
        todayBtn:  1,
  autoclose: 1,
  todayHighlight: 1,
  startView: 2,
  minView: 2,
  forceParse: 0,
  dateFormat:"YY-mm-dd"
    });
</script>
</body>
</html>
