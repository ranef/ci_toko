<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        TOKO JAYA ABADI
        <small>Jl. Raya Cilincing No.23</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Master</a></li>
        <li><a href="<?= base_url();?>karyawan/listkaryawan">Karyawan</a></li>
        <li class="active">Detail Karyawan</li>
      </ol>
    </section>
    <section class="content">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Karyawan</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
                    <tr align="center" bgcolor="#CCCCCC">
                <td><?php

              foreach($detailKaryawan as $data){
                $nik		=$data->nik;
                $nama_lengkap=$data->nama_lengkap;
                $tempat_lahir=$data->tempat_lahir;
                $tgl_lahir=$data->tgl_lahir;
                $jenis_kelamin=$data->jenis_kelamin;
                $alamat=$data->alamat;
                $telp=$data->telp;
                $kode_jabatan=$data->kode_jabatan;
                }

            ?>
                  NIK		:
                  <?=$nik;?>
                  <br/>
                  Nama	:
                  <?=$nama_lengkap;?>
                  <br/>
                  Tempat Lahir	:
                  <?=$tempat_lahir;?>
                  <br/>
                  Tanggal Lahir	:
                  <?=$tgl_lahir;?>
                  <br/>
                  Jenis Kelamin	:
                  <?=$jenis_kelamin;?>
                  <br/>
                  Alamat	:
                  <?=$alamat;?>
                  <br/>
                  Telp	:
                  <?=$telp;?>
                  <br/></td>
              </tr>
        </div>
      </div>
      <!-- /.box -->
    </section>
</div>
