<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        TOKO JAYA ABADI
        <small>Jl. Raya Cilincing No.23</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Master</a></li>
        <li><a href="<?= base_url();?>barang/listBarang">Barang</a></li>
        <li class="active">Detail Barang</li>
      </ol>
    </section>
    <section class="content">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Barang</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
                    <tr align="center" bgcolor="#CCCCCC">
                <td><?php

                        foreach($detailBarang as $data){
                        $kode_barang		=$data->kode_barang;
                        $nama_barang=$data->nama_barang;
                        $harga_barang=$data->harga_barang;
                        $kode_jenis=$data->kode_jenis;
                        }

                  ?>
                  <p>Kode Barang		:
                  <?=$kode_barang;?>
                  <br/>
                  Nama Barang	:
                  <?=$nama_barang;?>
                  <br/>
                  Harga	:
                  <?=$harga_barang;?>
                  <br/>
                  Kode Jenis	:
                  <?=$kode_jenis;?></p>
                  <br/></td>
              </tr>
        </div>
      </div>
      <!-- /.box -->
    </section>
</div>
