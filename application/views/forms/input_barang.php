<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        TOKO JAYA ABADI
        <small>Jl. Raya Cilincing No.23</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?= base_url();?>barang/listbarang"><i class="fa fa-dashboard"></i> Master</a></li>
        <li><a href="<?= base_url();?>barang/listbarang">Barang</a></li>
        <li class="active">Input Barang</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content" >
      <div class="box">
        <div class="col-md-6" >
          <!-- general form elements disabled -->
          <form class="form-general" action="<?=base_url()?>barang/inputbarang" method="POST" enctype="multipart/form-data" >
            <div class="box box-warning">
              <div class="box-header with-border">
                <h3 class="box-title">Input Barang</h3>
              </div>
              <!-- /.box-header -->
                <div class="box-body">
                  <!-- text input -->
                  <div class="form-group">
                    <label>KODE BARANG</label>
                    <input type="text" class="form-control" name="kode_barang" id="kode_barang" value="<?=$no_barang_urut;?>" readonly="on">
                  </div>
                  <div class="form-group">
                    <label>Nama Barang</label>
                    <input type="text" class="form-control" placeholder="Enter Name..." name="nama_barang" id="nama_barang" value="<?=set_value('nama_barang');?>">
                  </div>
                  <div class="form-group">
                    <label>Harga</label>
                    <input type="text" class="form-control" placeholder="Enter value..." name="harga_barang" id="harga_barang" value="<?=set_value('harga_barang');?>">
                  </div>
                  <div class="form-group">
                    <label>Jenis Barang</label>
                      <select class="form-control" style="width: 100%;" name="kode_jenis" id="kode_jenis">
                        <?php foreach($jenis_barang as $data) { ?>
                            <option value="<?=$data->kode_jenis;?>">
                              <?=$data->nama_jenis;?>
                            </option>
                        <?php } ?>
                      </select>
                  </div>
                  <div class="form-group">
                    <label>Stok</label>
                    <input type="text" class="form-control" placeholder="Enter Stok..." name="stok" id="stok" value="<?=set_value('stok');?>">
                  </div>
                  <div class="form-group">
                    <button type="reset" class="btn btn-default">Cancel</button>
                    <button type="submit" class="btn btn-info pull-right">Submit</button>
                  </div>
                
              </div>
              <!-- /.box-body -->
            </div>
          </form>
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <div class="input-group">
          <div class="input-group-addon">
            <i class="fa fa-clock-o"></i>
            <div align="center" style="color:red"><?=validation_errors();?></div>
          </div>           
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
</div>