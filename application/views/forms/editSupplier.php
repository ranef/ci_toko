<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        TOKO JAYA ABADI
        <small>Jl. Raya Cilincing No.23</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?= base_url();?>supplier/listSupplier"><i class="fa fa-dashboard"></i> Master</a></li>
        <li><a href="<?= base_url();?>supplier/listSupplier">Supplier</a></li>
        <li class="active">Edit Supplier</li>
      </ol>
    </section>
    <section class="content" >
      <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Supplier</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                      title="Collapse">
                <i class="fa fa-minus"></i></button>
              <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
              <?php
                foreach($editSupplier as $data){
                    $kode_supplier		=$data->kode_supplier;
                    $nama_supplier=$data->nama_supplier;
                    $alamat=$data->alamat;
                    $telp=$data->telp;
                    }
              ?>
            <form class="form" action="<?=base_url()?>supplier/editSupplier/<?=$kode_supplier;?>" method="POST" enctype="multipart/form-data" >
              <div class="box box-warning">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Supplier</h3>
                </div>
                <!-- /.box-header -->
                  <div class="box-body">
                    <!-- text input -->
                    <div class="form-group">
                      <label>Kode Supplier</label>
                      <input type="text" class="form-control" name="kode_supplier" id="kode_supplier" value="<?=$kode_supplier;?>" readonly="on">
                    </div>
                    <div class="form-group">
                      <label>Nama Suppier</label>
                      <input type="text" class="form-control" placeholder="Enter Name..." name="nama_supplier" id="nama_supplier" value="<?=$nama_supplier;?>">
                    </div>
                    <div class="form-group">
                      <label>Alamat</label>
                      <textarea class="form-control" rows="3" placeholder="Enter address ..." name="alamat" id="alamat"><?=$alamat;?></textarea>
                    </div>
                    <div class="form-group">
                      <label>Telp :</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-phone"></i>
                          </div>
                          <input type="text" class="form-control" data-inputmask='"mask": "(999) 999-9999"' data-mask maxlenght="15" name="telp" id="telp" value="<?=$telp;?>">
                        </div>
                    </div>
                    <div class="form-group">
                      <button type="reset" class="btn btn-default">Cancel</button>
                      <button type="submit" class="btn btn-info pull-right">Submit</button>
                    </div>
                  
              </div>
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-clock-o"></i>
                  <div align="center" style="color:red"><?=validation_errors();?></div>
                </div>           
              </div>
              <!-- /.box-body -->
            </div>
            </form>
        </div>
      </div>
      <!-- /.row -->
    </section>
</div>
