<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        TOKO JAYA ABADI
        <small>Jl. Raya Cilincing No.23</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Master</a></li>
        <li><a href="#">Report</a></li>
        <li class="active">Report Pembelian</li>
      </ol>
    </section>
    <section class="content">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Report Pembelian</h3>

          <div class="box-tools pull-right">
          </div>
        </div>
        <div class="box-body">
          <form action="<?= base_url();?>pembelian/listPembelian" method="POST">
            <div class="box-header">
              <h3 class="box-title">Dari Tanggal 
                  <?php echo $tgl_awal;?>
               s/d 
                  <?php echo $tgl_akhir;?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table">
                <tr>
                  <th style="width: 10px"><span class="label label-info"><a href="<?=base_url();?>report/cetakPDf/<?=$tgl_awal;?>/<?=$tgl_akhir;?>">Cetak PDF</a></th>
                </tr>
                <tr>
                  <th style="width: 10px">No</th>
                  <th>Id Pembelian</th>
                  <th>Nomer Transaksi</th>
                  <th>Tanggal</th>
                  <th>Total Barang</th>
                  <th>Total Qty</th>
                  <th>Total Nominal Pembelian</th>
                  <th>Jumlah</th>
                </tr>
                <?php
                  $total_hitung=0;
                  $no_urut = 0;
                  foreach ($report_header as $data)
                  {
                  $no_urut++;
	            	?>
                  
                <tr>
                    <th><?=$no_urut;?></th>
                    <th><?=$data->id_pembelian_h;?></th>
                    <th><?=$data->no_transaksi;?></th>
                    <th><?=$data->tanggal;?></th>
                    <th><?= number_format($data->total_barang);?></th>
                    <th><?= number_format($data->total_qty);?></th>
                    <th><?= number_format($data->harga);?></th> 
                    <th><?= number_format($data->total_pembelian);?></th> 
                </tr>
                   <?php 
                     $total_hitung += $data->total_pembelian;} 
                    ?>
                <tr>
                    <th colspan="7">JUMLAH :</th>
                    <th>Rp. <b><?= number_format($total_hitung); ?></th>
                </tr>
              </table>
    
              </table>
            </div>
              <tr>
                <th colspan="7" align="center"></th>
              </tr>
              <div class="box-footer clearfix">
                <ul class="pagination pagination-sm no-margin center">
                  <li><?= $this->pagination->create_links();?></li>
                </ul>
              </div>
            </div>
            </div>
          </form>
        </div>
      </div>
      <!-- /.box -->
      </section>
</div>
