<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        TOKO JAYA ABADI
        <small>Jl. Raya Cilincing No.23</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Master</a></li>
        <li><a href="#">Supplier</a></li>
        <li class="active">List Supplier</li>
      </ol>
    </section>
    <section class="content">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Supplier</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <form action="<?= base_url();?>supplier/listSupplier" method="POST">
            <div class="box-header">
              <h3 class="box-title">List Supplier</h3>
                <?php 
			            if($this->session->flashdata('info')==true){
				          echo $this->session->flashdata('info');
				        }?>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table">
                <tr>
                  <th style="width: 10px">No</th>
                  <th>Kode Supplier</th>
                  <th>Nama Supplier</th>
                  <th>Alamat</th>
                  <th>Telp</th>
                  <th style="width: 40px" colspan="3">AKSI</th>
                </tr>  
                  <ul class="pagination pagination-sm no-margin pull-left">
                            <li><span class="label label-warning"><a href="<?=base_url();?>supplier/inputsupplier" >Input Data</a></span>
                            </li>
                  </ul>
                  <ul class="pagination pagination-sm no-margin pull-right">
                            <li><input type="text" name="cari" placeholder="Cari Nama" />
                            <input type="submit" name="tombol_cari" id="cari_data" value="Cari Nama" class="btn btn-primary"/>
                            </li>
                  </ul>
                    <?php
                      $data_posisi=$this->uri->segment(4);
                      $no_urut=$data_posisi;
                      if(count($data_supplier)>0){
                      foreach ($data_supplier as $data){
                      $no_urut++;

                    ?>
                <tr>
                  <td><?=$no_urut;?></td>
                  <td><?=$data->kode_supplier;?></td>
                  <td><?=$data->nama_supplier;?></td>
                  <td><?=$data->alamat;?></td>
                  <td><?=$data->telp;?></td> 
                  <td><span class="label label-info"><a href="<?=base_url();?>supplier/detailSupplier/<?=$data->kode_supplier;?>">detail</a></td>
                  <td><span class="label label-success"><a href="<?=base_url();?>supplier/editSupplier/<?=$data->kode_supplier;?>">edit</a></span></td>
                  <td><span class="label label-danger"><a href="<?=base_url();?>/supplier/deleteSupplier/<?=$data->kode_supplier;?>" onclick="return confirm('Yakin ingin menghapus data?');">Delete</a></span></td>
                </tr>
                   <?php } ?>
              </table>
                        <?php }else{?>
              <table>
                <div class="pad margin no-print">
                    <div class="callout callout-info" style="margin-bottom: 0!important;">
                      <h4><i class="fa fa-info"></i> Note:</h4>
                          Data Not Found.. Please Try Again ..
                    </div>
                </div>
              </table>
                        <?php }?>
              </table>
            </div>
                <tr>
                  <td colspan="7" align="center"></td>
                </tr>
            <div class="box-footer clearfix">
              <ul class="pagination pagination-sm no-margin center">
                 <li><?= $this->pagination->create_links();?></li>
              </ul>
          </div>
        </div>
    </form>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Footer
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>



