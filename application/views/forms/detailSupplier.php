<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        TOKO JAYA ABADI
        <small>Jl. Raya Cilincing No.23</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Master</a></li>
        <li><a href="<?= base_url();?>supplier/listSupplier">Supplier</a></li>
        <li class="active">Detail Supplier</li>
      </ol>
    </section>
    <section class="content">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Supplier</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
                    <tr align="center" bgcolor="#CCCCCC">
                <td><?php
                    foreach($detailSupplier as $data){
                        $kode_supplier		=$data->kode_supplier;
                        $nama_supplier=$data->nama_supplier;
                        $alamat=$data->alamat;
                        $telp=$data->telp;
                        }
                ?>
                    Kode Supplier		:<?=$kode_supplier;?>
                    <br/>
                    Nama Supplier		:
                    <?=$nama_supplier;?>
                    <br/>
                    Alamat				:
                    <?=$alamat;?>
                    <br/>
                    No Telp				:
                    <?=$telp;?>
                    <br/></td>
              </tr>
        </div>
      </div>
      <!-- /.box -->
    </section>
</div>
