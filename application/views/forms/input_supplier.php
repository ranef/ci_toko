<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        TOKO JAYA ABADI
        <small>Jl. Raya Cilincing No.23</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?= base_url();?>supplier/listsupplier"><i class="fa fa-dashboard"></i> Master</a></li>
        <li><a href="<?= base_url();?>supplier/listsupplier">Supplier</a></li>
        <li class="active">Input Supplier</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content" >
      <div class="box">
        <div class="col-md-6" >
          <!-- general form elements disabled -->
        <form class="form-general" action="<?=base_url()?>supplier/inputsupplier" method="POST" enctype="multipart/form-data" >
            <div class="box box-warning">
              <div class="box-header with-border">
                <h3 class="box-title">Input Supplier</h3>
              </div>
              <!-- /.box-header -->
                <div class="box-body">
                  <!-- text input -->
                  <div class="form-group">
                    <label>Kode Supplier</label>
                    <input type="text" class="form-control" name="kode_supplier" id="kode_supplier" value="<?=$no_urut_kode_supplier;?>" readonly="on">
                  </div>
                  <div class="form-group">
                    <label>Nama Supplier</label>
                    <input type="text" class="form-control" placeholder="Enter Name..." name="nama_supplier" id="nama_supplier">
                  </div>
                  <div class="form-group">
                    <label>Alamat</label>
                    <textarea class="form-control" rows="3" placeholder="Enter address ..." name="alamat" id="alamat"></textarea>
                  </div>
                  <div class="form-group">
                    <label>Telp :</label>
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-phone"></i>
                        </div>
                        <input type="text" class="form-control" data-inputmask='"mask": "(999) 999-9999"' data-mask maxlenght="15" name="telp" id="telp">
                      </div>
                  </div>
                  <div class="form-group">
                    <button type="reset" class="btn btn-default">Cancel</button>
                    <button type="submit" class="btn btn-info pull-right">Submit</button>
                  </div>  
              </div>
              <!-- /.box-body -->
            </div>
          </form>
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <div class="input-group">
          <div class="input-group-addon">
            <i class="fa fa-clock-o"></i>
            <div align="center" style="color:red"><?=validation_errors();?></div>
          </div>           
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
</div>