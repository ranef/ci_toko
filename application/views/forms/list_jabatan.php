<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        TOKO JAYA ABADI
        <small>Jl. Raya Cilincing No.23</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Master</a></li>
        <li><a href="#">Jabatan</a></li>
        <li class="active">List Jabatan</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Jabatan</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
        <form action="<?= base_url();?>jabatan/listJabatan" method="POST">
            <div class="box-header">
              <h3 class="box-title">List Jabatan</h3>
                <?php 
			            if($this->session->flashdata('info')==true){
				          echo $this->session->flashdata('info');
				        }?>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table">
                  <ul class="pagination pagination-sm no-margin pull-left">
                            <li><span class="label label-warning"><a href="<?=base_url();?>jabatan/inputjabatan" >Input Data</a></span>
                            </li>
                  </ul>
                  <ul class="pagination pagination-sm no-margin pull-right">
                            <li><input type="text" name="cari" placeholder="Cari Nama" />
                            <input type="submit" name="tombol_cari" id="cari_data" value="Cari Nama" class="btn btn-primary"/>
                            </li>
                  </ul>
                <tr>
                  <th style="width: 10px">No</th>
                  <th>Kode Jabatan</th>
                  <th>Nama Jabatan</th>
                  <th>Keterangan</th>
                  <th style="width: 40px" colspan="3">AKSI</th>
                </tr>
                <?php
                    $data_posisi=$this->uri->segment(4);
                    $no_urut=$data_posisi;
                    if(count($data_jabatan)>0){
		 		            foreach ($data_jabatan as $data){
			            	$no_urut++;

		                ?>
                <tr>
                  <td><?=$no_urut;?></td>
                  <td><?=$data->kode_jabatan;?></td>
                  <td><?=$data->nama_jabatan;?></td>
                  <td><?=$data->keterangan;?></td>   
                  <td><span class="label label-info"><a href="<?=base_url();?>jabatan/detailJabatan/<?=$data->kode_jabatan;?>">detail</a></td>
                  <td><span class="label label-success"><a href="<?=base_url();?>jabatan/editJabatan/<?=$data->kode_jabatan;?>">edit</a></span></td>
                  <td><span class="label label-danger"><a href="<?=base_url();?>/jabatan/deleteJabatan/<?=$data->kode_jabatan;?>" onclick="return confirm('Yakin ingin menghapus data?');">Delete</a></span></td>
                </tr>
                   <?php } ?>
              </table>
                        <?php }else{?>
                <table>
                  <div class="pad margin no-print">
                    <div class="callout callout-info" style="margin-bottom: 0!important;">
                      <h4><i class="fa fa-info"></i> Note:</h4>
                          Data Not Found.. Please Try Again ..
                    </div>
                  </div>
                </table>
                        <?php }?>
              </table>
            </div>
              <tr>
                <td colspan="7" align="center"></td>
              </tr>
              <div class="box-footer clearfix">
                <ul class="pagination pagination-sm no-margin center">
                  <li><?= $this->pagination->create_links();?></li>
                </ul>
              </div>
          </div>
          </div>
        </form>
        </div>
        <!-- /.box-body -->
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>



