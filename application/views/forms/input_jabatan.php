<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        TOKO JAYA ABADI
        <small>Jl. Raya Cilincing No.23</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?= base_url();?>jabatan/listjabatan"><i class="fa fa-dashboard"></i> Master</a></li>
        <li><a href="<?= base_url();?>jabatan/listjabatan">jabatan</a></li>
        <li class="active">Input Jabatan</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content" >
      <div class="box">
        <div class="col-md-6" >
          <!-- general form elements disabled -->
          <form class="form-general" action="<?=base_url()?>jabatan/inputjabatan" method="POST" enctype="multipart/form-data" >
            <div class="box box-warning">
              <div class="box-header with-border">
                <h3 class="box-title">Input Jabatan</h3>
              </div>
              <!-- /.box-header -->
                <div class="box-body">
                  <!-- text input -->
                  <div class="form-group">
                    <label>Kode Jabatan</label>
                    <input type="text" class="form-control" name="kode_jabatan" id="kode_jabatan" value="<?=$no_urut_jabatan;?>" readonly="on">
                  </div>
                  <div class="form-group">
                    <label>Nama Jabatan</label>
                    <input type="text" class="form-control" placeholder="Enter Name..." name="nama_jabatan" id="nama_jabatan">
                  </div>
                  <div class="form-group">
                    <label>Keterangan</label>
                    <input type="text" class="form-control" placeholder="Enter Note..." name="keterangan" id="keterangan">
                  </div>
                  <div class="form-group">
                    <button type="reset" class="btn btn-default">Cancel</button>
                    <button type="submit" class="btn btn-info pull-right">Submit</button>
                  </div>
                
              </div>
              <!-- /.box-body -->
            </div>
          </form>
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <div class="input-group">
          <div class="input-group-addon">
            <i class="fa fa-clock-o"></i>
            <div align="center" style="color:red"><?=validation_errors();?></div>
          </div>           
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
</div>