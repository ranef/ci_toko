<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        TOKO JAYA ABADI
        <small>Jl. Raya Cilincing No.23</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Master</a></li>
        <li><a href="#">Barang</a></li>
        <li class="active">List Barang</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Barang</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
        <form action="<?= base_url();?>jabatan/listJabatan" method="POST">
            <div class="box-header">
              <h3 class="box-title">List Barang</h3>
                <?php 
			            if($this->session->flashdata('info')==true){
				          echo $this->session->flashdata('info');
				        }?>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table">
                <tr>
                  <th style="width: 10px">No</th>
                  <th>Kode Barang</th>
                  <th>Nama Barang</th>
                  <th>Harga Barang</th>
                  <th>Nama Jenis</th>
                  <th>Stok</th>
                  <th style="width: 40px" colspan="3">AKSI</th>
                </tr>
                <?php
                    $data_posisi=$this->uri->segment(4);
                    $no_urut=$data_posisi;
                    if(count($data_barang)>0){
		 		            foreach ($data_barang as $data){
			            	$no_urut++;

		                ?>
                <tr>
                  <td><?=$no_urut;?></td>
                  <td><?=$data->kode_barang;?></td>
                  <td><?=$data->nama_barang;?></td>
                  <td><?=$data->harga_barang;?></td>
                  <td><?=$data->nama_jenis;?></td>
                  <td><?=$data->stok;?></td>   
                  <td><span class="label label-info"><a href="<?=base_url();?>barang/detailBarang/<?=$data->kode_barang;?>">detail</a></td>
                  <td><span class="label label-success"><a href="<?=base_url();?>barang/editBarang/<?=$data->kode_barang;?>">edit</a></span></td>
                  <td><span class="label label-danger"><a href="<?=base_url();?>/barang/deleteBarang/<?=$data->kode_barang;?>" onclick="return confirm('Yakin ingin menghapus data?');">Delete</a></span></td>
                </tr>
                   <?php } ?>
                <tr>  
                  <ul class="pagination pagination-sm no-margin pull-left">
                            <li><span class="label label-warning"><a href="<?=base_url();?>barang/inputbarang" >Input Data</a></span>
                            </li>
                  </ul>
                  <ul class="pagination pagination-sm no-margin pull-right">
                            <li><input type="text" name="cari" placeholder="Cari Nama" />
                            <input type="submit" name="tombol_cari" id="cari_data" value="Cari Nama" class="btn btn-primary"/>
                            </li>
                  </ul>
                </tr>
              </table>
                        <?php }else{?>
              <table>
                <div class="pad margin no-print">
                    <div class="callout callout-info" style="margin-bottom: 0!important;">
                      <h4><i class="fa fa-info"></i> Note:</h4>
                          Data Not Found.. Please Try Again ..
                    </div>
                </div>
              </table>
                        <?php }?>
              </table>
            </div>
            <tr>
          <td colspan="7" align="center"></td>
          </tr>
          </div>
          <div class="box-footer clearfix">
              <ul class="pagination pagination-sm no-margin center">
                 <li><?= $this->pagination->create_links();?></li>
              </ul>
          </div>
        </div>
  </form>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Footer
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>



