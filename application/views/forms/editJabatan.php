<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        TOKO JAYA ABADI
        <small>Jl. Raya Cilincing No.23</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Master</a></li>
        <li><a href="<?= base_url();?>jabatan/listjabatan">Jabatan</a></li>
        <li class="active">Edit Jabatan</li>
      </ol>
    </section>
    <section class="content" >
      <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Jabatan</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                      title="Collapse">
                <i class="fa fa-minus"></i></button>
              <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
              <?php
                foreach($editJabatan as $data){
                  $kode_jabatan=$data->kode_jabatan;
                  $nama_jabatan=$data->nama_jabatan;
                  $keterangan=$data->keterangan;
                  
                    }
              ?>
            <form class="form" action="<?=base_url()?>jabatan/editJabatan/<?=$kode_jabatan;?>" method="POST" enctype="multipart/form-data" >
              <div class="box box-warning">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Jabatan</h3>
                </div>
                <!-- /.box-header -->
                  <div class="box-body">
                    <!-- text input -->
                    <div class="form-group">
                      <label>Kode Jabatan</label>
                      <input type="text" class="form-control" name="kode_jabatan" id="kode_jabatan" value="<?=$kode_jabatan;?>" readonly="on">
                    </div>
                    <div class="form-group">
                      <label>Nama Jabatan</label>
                        <select class="form-control" style="width: 100%;" name="nama_jabatan" id="nama_jabatan">
                        <?php foreach($editJabatan as $data) {$select_jabatan=($data->kode_jabatan==$kode_jabatan)?'selected':'';?>
                          <option value="<?=$data->kode_jabatan;?>" <?=$select_jabatan;?>><?=$data->nama_jabatan;?>
                          </option>
                          <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                      <label>Alamat</label>
                      <textarea class="form-control" rows="3" placeholder="Enter Note ..." name="keterangan" id="keterangan"><?=$keterangan;?></textarea>
                    </div>
                    <div class="form-group">
                      <button type="reset" class="btn btn-default">Cancel</button>
                      <button type="submit" class="btn btn-info pull-right">Submit</button>
                    </div>                
              </div>
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-clock-o"></i>
                  <div align="center" style="color:red"><?=validation_errors();?></div>
                </div>           
              </div>
              <!-- /.box-body -->
            </div>
            </form>
        </div>
      </div>
      <!-- /.row -->
    </section>
</div>
