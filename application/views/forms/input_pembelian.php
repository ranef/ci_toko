<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        TOKO JAYA ABADI
        <small>Jl. Raya Cilincing No.23</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?= base_url();?>pembelian/listpembelian"><i class="fa fa-dashboard"></i> Master</a></li>
        <li><a href="<?= base_url();?>pembelian/listpembelian">Pembelian</a></li>
        <li class="active">Input Pembelian</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content" >
      <div class="box">
          <!-- general form elements disabled -->
          <form class="form-general" action="<?=base_url()?>pembelian/input_h" method="POST" enctype="multipart/form-data" >
              <div class="box-header with-border">
                <h3 class="box-title">Input pembelian</h3>
              </div>
              <div class="input-group-addon">
                <i class="fa fa-clock-o"></i>
                <div align="center" style="color:red"><?=validation_errors();?></div>
              </div>       
              <!-- /.box-header -->
                <div class="box-body">
                  <!-- text input -->
                  <div class="form-group">
                    <label>Nomer Transaksi</label>
                    <input type="text" class="form-control" name="no_transaksi" id="no_transaksi" value="<?=$no_urut_transaksi;?>" readonly="on">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Nama Supplier</label>
                        <select class="form-control select2" style="width: 100%;" name="kode_supplier" id="kode_supplier">
                        <?php foreach($data_supplier as $data) { ?>
                            <option value="<?=$data->kode_supplier;?>"><?=$data->nama_supplier;?></option>
                        <?php } ?>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <button type="reset" class="btn btn-default">Cancel</button>
                    <button type="submit" class="btn btn-info pull-right">Submit</button>
                  </div>
                
              </div>
              <!-- /.box-body -->
            
          </form>
          <!-- /.box -->
        
        <!--/.col (right) -->
      </div>
      <div class="input-group">
              
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
</div>