<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        TOKO JAYA ABADI
        <small>Jl. Raya Cilincing No.23</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?= base_url();?>report/"><i class="fa fa-dashboard"></i> Master</a></li>
        <li><a href="<?= base_url();?>report/">Report</a></li>
        <li class="active">Report Penjualan</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content" >
      <div class="box">
          <!-- general form elements disabled -->
          <form class="form-general" action="<?=base_url()?>report/report_pen/?>" method="POST" enctype="multipart/form-data" >
              <div class="box-header with-border">
                <h3 class="box-title">Report Penjualan</h3>
              </div>
              <div class="input-group-addon">
                <i class="fa fa-clock-o"></i>
              </div>       
              <div class="form-group">
                    <label>Tanggal Awal Transaksi:</label>
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control pull-right" id="datepicker" name="tanggal_awal" data-date-format="yyyy-mm-dd" autocomplete="off">
                      </div>
                  </div>
                  <div class="form-group">
                    <label>Tanggal Akhir Transaksi:</label>
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control pull-right" id="datepicker1" name="tanggal_akhir" data-date-format="yyyy-mm-dd" autocomplete="off">
                      </div>
                  </div>
                  <div class="form-group">
                    <button type="reset" class="btn btn-default">Cancel</button>
                    <button type="submit" class="btn btn-info pull-right">Submit</button>
                  </div>
                
              </div>
              </div>
              
              <!-- /.box-body -->
            
          </form>
          <!-- /.box -->
        
        <!--/.col (right) -->
      </div>
      <div class="input-group">
              
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
</div>