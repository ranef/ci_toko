<form action="<?=base_url()?>pembelian/input_d/<?= $id_header; ?>" method="POST">
<table width="100%" border="0" align="center">
  <tr>
    <td><table width="100%" border="0">
    <tr align="center">
        <td height="10">&nbsp;</td>
      </tr>
      <tr align="center">
        <td height="80"><h2>Input Pembelian</h2></td>
      </tr>
      <div align="center" style="color:red"><?=validation_errors();?></div>
      <tr>
        <td><table width="50%" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="">
          <tr>
            <td>Nama Barang</td>
            <td>:</td>
            <td><select name="kode_barang" id="kode_barang">
              <?php foreach($data_barang as $data) { ?>
              <option value="<?=$data->kode_barang;?>">
                <?=$data->nama_barang;?>
                </option>
              <?php } ?>
            </select></td>
          </tr>
          <tr>
            <td>Qty</td>
            <td> :</td>
            <td><input type="text" name="qty" id="qty" maxlength="5"></td>
          </tr>
          <tr>
            <td>Harga Barang</td>
            <td>:</td>
            <td><input type="text" name="harga" id="harga" maxlength="100" value="<?=$data->harga_barang;?>"></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>
              <input type="submit" name="post" id="post" value="Proses">
            </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td align="left"><a href="<?=base_url();?>pembelian/input_h">
      <input type="button" name="Kembali" id="Kembali" value="Kembali Ke Menu Sebelumnya">
    </a>
    <?php 
			if($this->session->flashdata('info')==true){
				echo $this->session->flashdata('info');
				}?>
    </td>
          </tr>
          <tr>
          
          </tr>
          </table>
          <table width="100%" border="0">
            <tr align="center">
              <td height="10"><h2>&nbsp;</h2></td>
            </tr>
            <tr bgcolor="#CCCCCC">
              <td><table width="100%" border="0">
                <tr align="center">
                  <td bgcolor="#3366FF">No</td>
                  <td bgcolor="#3366FF">Kode Barang</td>
                  <td bgcolor="#3366FF">Nama Barang</td>
                  <td bgcolor="#3366FF">Qty</td>
                  <td bgcolor="#3366FF">Harga</td>
                  <td bgcolor="#3366FF">Jumlah</td>
                </tr>
                <?php
        $total_hitung=0;
		 		$no_urut = 0;
		 		foreach ($data_pembelian_detail as $data)
        {
				$no_urut++;
		?>
                <tr align="center" bgcolor="#000000">
                  <td><font color="#FFFFFF">
                    <?= $no_urut;?>
                    </font></td>
                  <td><font color="#FFFFFF">
                    <?= $data->kode_barang;?>
                    </font></td>
                  <td><font color="#FFFFFF">
                    <?= $data->nama_barang;?>
                    </font></td>
                    <td><font color="#FFFFFF">
                    <?= $data->qty;?>
                    </font></td>
                    <td><font color="#FFFFFF">
                    <?= number_format($data->harga);?>
                    </font></td>
                  <td><<font color="#FFFFFF">
                    <?= number_format($data->jumlah);?>
                    </font></td>
                </tr>
                  <?php
                  // hitung total
                  $total_hitung += $data->jumlah;
                   } 
                  ?>

                <tr align="center" bgcolor="#000000" >
                  <td colspan="5" align="right" bgcolor="#FFFFFF">TOTAL</td>
                  <td align="right">
                  <font color="#FFFFFF" >
                  Rp. <b><?= number_format($total_hitung); ?></b> </font></td>
                </tr>
                </table></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
  </form>
