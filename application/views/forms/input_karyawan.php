<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        TOKO JAYA ABADI
        <small>Jl. Raya Cilincing No.23</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?= base_url();?>karyawan/listkaryawan"><i class="fa fa-dashboard"></i> Master</a></li>
        <li><a href="<?= base_url();?>karyawan/listkaryawan">Karyawan</a></li>
        <li class="active">Input Karyawan</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content" >
      <div class="box">
          <!-- general form elements disabled -->
          <form class="form-general" action="<?=base_url()?>karyawan/inputkaryawan" method="POST" enctype="multipart/form-data" >
              <div class="box-header with-border">
                <h3 class="box-title">Input Karyawan</h3>
              </div>
              <div class="input-group-addon">
                <i class="fa fa-clock-o"></i>
                <div align="center" style="color:red"><?=validation_errors();?></div>
              </div>       
              <!-- /.box-header -->
                <div class="box-body">
                  <!-- text input -->
                  <div class="form-group">
                    <label>NIK</label>
                    <input type="text" class="form-control" name="nik" id="nik" value="<?=$no_urut_nik;?>" readonly="on">
                  </div>
                  <div class="form-group">
                    <label>Nama</label>
                    <input type="text" class="form-control" placeholder="Enter Name..." name="nama_karyawan" id="nama_karyawan">
                  </div>
                  <div class="form-group">
                    <label>Tempat Lahir</label>
                    <input type="text" class="form-control" placeholder="Enter Name Of Place..." name="tempat_lahir" id="tempat_lahir">
                  </div>
                  <div class="form-group">
                    <label>Jenis Kelamin</label>
                      <select class="form-control" style="width: 100%;" name="jenis_kelamin" id="jenis_kelamin">
                        <option selected="selected" value="P">Perempuan</option>
                        <option value="L">Laki-Laki</option>
                      </select>
                  </div>
                  <div class="form-group">
                    <label>Tanggal Lahir:</label>
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control pull-right" id="datepicker" name="tanggal" data-date-format="yyyy-mm-dd" autocomplete="off">
                      </div>
                  </div>
                  <div class="form-group">
                    <label>Telp :</label>
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-phone"></i>
                        </div>
                        <input type="text" class="form-control" data-inputmask='"mask": "(999) 999-9999"' data-mask maxlenght="15" name="telp" id="telp">
                      </div>
                  </div>  
                  <div class="form-group">
                    <label>Alamat</label>
                    <textarea class="form-control" rows="3" placeholder="Enter address ..." name="alamat" id="alamat"></textarea>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Jabatan</label>
                        <select class="form-control select2" style="width: 100%;" name="nama_jabatan" id="nama_jabatan">
                          <?php foreach($data_jabatan as $data) { ?>
                          <option selected="selected" value="<?=$data->kode_jabatan;?>"><?=$data->nama_jabatan;?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputFile">Foto</label>
                    <input type="file" id="exampleInputFile" name="image">
                    <p class="help-block">Upload Your Image Here.</p>
                  </div>
                  <div class="form-group">
                    <button type="reset" class="btn btn-default">Cancel</button>
                    <button type="submit" class="btn btn-info pull-right">Submit</button>
                  </div>
                
              </div>
              <!-- /.box-body -->
            
          </form>
          <!-- /.box -->
        
        <!--/.col (right) -->
      </div>
      <div class="input-group">
              
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
</div>