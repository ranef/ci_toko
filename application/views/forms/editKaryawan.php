<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        TOKO JAYA ABADI
        <small>Jl. Raya Cilincing No.23</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?= base_url();?>karyawan/listkaryawan"><i class="fa fa-dashboard"></i> Master</a></li>
        <li><a href="<?= base_url();?>karyawan/listkaryawan">Karyawan</a></li>
        <li class="active">Edit Karyawan</li>
      </ol>
    </section>
    <section class="content" >
      <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Karyawan</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                      title="Collapse">
                <i class="fa fa-minus"></i></button>
              <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
              <?php
                foreach($editKaryawan as $data){
                  $nik		=$data->nik;
                  $nama_lengkap=$data->nama_lengkap;
                  $tempat_lahir=$data->tempat_lahir;
                  $jenis_kelamin=$data->jenis_kelamin;
                  $alamat=$data->alamat;
                  $telp=$data->telp;
                  $kode_jabatan=$data->kode_jabatan;
                  $tgl_lahir=$data->tgl_lahir;
                  $foto =$data->foto;
                  }
                  $thn_pisah=substr($tgl_lahir,0,4);
                  $bln_pisah=substr($tgl_lahir,5,2);
                  $tgl_pisah=substr($tgl_lahir,8,2);
              ?>
            <form class="form" action="<?=base_url()?>karyawan/editKaryawan/<?=$nik;?>" method="POST" enctype="multipart/form-data" >
              <div class="box box-warning">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Karyawan</h3>
                </div>
                <!-- /.box-header -->
                  <div class="box-body">
                    <!-- text input -->
                    <div class="form-group">
                      <label>NIK</label>
                      <input type="text" class="form-control" name="nik" id="nik" value="<?=$nik;?>" readonly="on">
                    </div>
                    <div class="form-group">
                      <label>Nama</label>
                      <input type="text" class="form-control" placeholder="Enter Name..." name="nama_karyawan" id="nama_karyawan" value="<?=$nama_lengkap;?>">
                    </div>
                    <div class="form-group">
                      <label>Tempat Lahir</label>
                      <input type="text" class="form-control" placeholder="Enter Name Of Place..." name="tempat_lahir" id="tempat_lahir" value="<?=$tempat_lahir;?>">
                    </div>
                    <div class="form-group">
                      <label>Jenis Kelamin</label>
                        <select class="form-control" style="width: 100%;" name="jenis_kelamin" id="jenis_kelamin">
                          <?php
                                $jk=['L','P'];
                                $jk_text=['L'=>'Laki-Laki','P'=>'Perempuan'];
                                foreach ($jk as $data) {
                                  if($data==$jenis_kelamin){
                                  $select="selected";
                                } else{
                                  $select='';
                                }
                          ?>
                          <option value="<?=$data;?>" <?=$select;?>><?=$jk_text[$data];?></option>
                          <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                      <label>Tanggal Lahir:</label>
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" class="form-control pull-right" id="datepicker" name="tanggal" data-date-format="yyyy-mm-dd" autocomplete="off" value="<?=$tgl_lahir;?>">
                        </div>
                    </div>
                    <div class="form-group">
                      <label>Telp :</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-phone"></i>
                          </div>
                          <input type="text" class="form-control" data-inputmask='"mask": "(999) 999-9999"' data-mask maxlenght="15" name="telp" id="telp" value="<?=$telp;?>">
                        </div>
                    </div>  
                    <div class="form-group">
                      <label>Alamat</label>
                      <textarea class="form-control" rows="3" placeholder="Enter address ..." name="alamat" id="alamat"><?=$alamat;?></textarea>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Jabatan</label>
                          <select class="form-control select2" style="width: 100%;" name="nama_jabatan" id="nama_jabatan">
                            <?php foreach($data_jabatan as $data) {$select_jabatan=($data->kode_jabatan==$kode_jabatan)?'selected':'';?>
                            <option selected="selected" value="<?=$data->kode_jabatan;?>" <?=$select_jabatan;?>><?=$data->nama_jabatan;?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputFile">Foto</label>
                      <input type="file" id="exampleInputFile" name="image">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?=base_url()?>/resources/fotokaryawan/<?=$foto;?>" class="user-image" alt="User Image">
                      </a>
                      <input type="hidden" id="exampleInputFile" name="foto_old" id="foto_old" value="<?=$foto;?>">
                      <p class="help-block">Change Your Image Here.</p>
                    </div>
                    <div class="form-group">
                      <button type="reset" class="btn btn-default">Cancel</button>
                      <button type="submit" class="btn btn-info pull-right">Submit</button>
                    </div>
                  
              </div>
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-clock-o"></i>
                  <div align="center" style="color:red"><?=validation_errors();?></div>
                </div>           
              </div>
              <!-- /.box-body -->
            </div>
            </form>
        </div>
      </div>
      <!-- /.row -->
    </section>
</div>
