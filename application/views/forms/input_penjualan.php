<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        TOKO JAYA ABADI
        <small>Jl. Raya Cilincing No.23</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?= base_url();?>penjualan/listpenjualan"><i class="fa fa-dashboard"></i> Master</a></li>
        <li><a href="<?= base_url();?>penjualan/listpenjualan">Penjualan</a></li>
        <li class="active">Input Penjualan</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content" >
      <div class="box">
          <!-- general form elements disabled -->
          <form class="form-general" action="<?=base_url()?>penjualan/input_h" method="POST" enctype="multipart/form-data" >
              <div class="box-header with-border">
                <h3 class="box-title">Input Penjualan</h3>
              </div>
              <div class="input-group-addon">
                <i class="fa fa-clock-o"></i>
                <div align="center" style="color:red"><?=validation_errors();?></div>
              </div>       
              <!-- /.box-header -->
                <div class="box-body">
                  <!-- text input -->
                  <div class="form-group">
                    <label>Nomer Transaksi</label>
                    <input type="text" class="form-control" name="no_transaksi" id="no_transaksi" value="<?=$no_urut_transaksi;?>">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Nama Pembeli</label>
                        <input type="text" class="form-control" name="pembeli" id="pembeli">
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <button type="reset" class="btn btn-default">Cancel</button>
                    <button type="submit" class="btn btn-info pull-right">Submit</button>
                  </div>
                
              </div>
              <!-- /.box-body -->
            
          </form>
          <!-- /.box -->
        
        <!--/.col (right) -->
      </div>
      <div class="input-group">
              
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
</div>