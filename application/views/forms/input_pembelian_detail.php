<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        TOKO JAYA ABADI
        <small>Jl. Raya Cilincing No.23</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?= base_url();?>pembelian/listpembelian"><i class="fa fa-dashboard"></i> Master</a></li>
        <li><a href="<?= base_url();?>pembelian/listpembelian">Pembelian</a></li>
        <li><a href="<?= base_url();?>pembelian/input_h">Input Pembelian</a></li>
        <li class="active">Input Pembelian Detail</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content" >
      <div class="box">
          <!-- general form elements disabled -->
          <form class="form-general" action="<?=base_url()?>pembelian/input_d/<?= $id_header; ?>" method="POST" enctype="multipart/form-data" >
              <div class="box-header with-border">
                <h3 class="box-title">Input Pembelian Detail </h3>
              </div>
              <div class="input-group-addon">
                <i class="fa fa-clock-o"></i>
                <div align="center" style="color:red"><?=validation_errors();?></div>
              </div>       
              <!-- /.box-header -->
                <div class="box-body">
                  <!-- text input -->
                  <div class="form-group">
                        <label>Nama Barang</label>
                        <select class="form-control select2" style="width: 100%;" name="kode_barang" id="kode_barang" onkeyup="isi_otomatis()">
                          <?php foreach($data_barang as $data) {?>
                            <option value="<?=$data->kode_barang;?>">
                            <?=$data->nama_barang;?>
                            </option>
                            <?php } ?>
                        </select>
                      </div>
                  </div>
                  <div class="box-body">
                    <div class="form-group">
                      <label>Qty</label>
                      <input type="text" class="form-control" name="qty" id="qty">
                    </div>
                  </div>
                  <div class="box-body">
                    <div class="form-group">
                      <button type="submit" class="btn btn-info">Submit</button>
                    </div>
                  </div>
                  <?php 
                  if($this->session->flashdata('info')==true){
                    echo $this->session->flashdata('info');
                    }?>
                    <div class="box-body no-padding">
                  <table class="table">
                    <tr>
                      <th style="width: 10px">No</th>
                      <th>Kode Barang</th>
                      <th>Nama Barang</th>
                      <th>Qty</th>
                      <th>Harga</th>
                      <th>Jumlah</th>
                    </tr>
                      <?php
                        $total_hitung=0;
                        $no_urut = 0;
                        $harga=0;
                        foreach ($data_pembelian_detail as $data)
                        {
                        $no_urut++;
                      ?>  
                    <tr>
                      <td><?=$no_urut;?></td>
                      <td><?=$data->kode_barang;?></td>
                      <td><?=$data->nama_barang;?></td>
                      <td><?=$data->qty;?></td>
                      <td><?= number_format($data->harga);?></td>
                      <td><?= number_format($data->jumlah);?></td>
                        <?php
                          // hitung total
                          $total_hitung += $data->jumlah;
                          } 
                        ?>    
                      </td>
                    </tr>
                    <tr>
                      <th style="width: 10px" colspan="5">Total</th>
                      <th>Rp. <b><?= number_format($total_hitung); ?></b></th>
                    </tr>
                  </table>
                </div>
              </div>
          </form>
          <!-- /.box -->
        
        <!--/.col (right) -->
      </div>
      <div class="input-group">
              
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
</div>