<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        TOKO JAYA ABADI
        <small>Jl. Raya Cilincing No.23</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Master</a></li>
        <li><a href="<?= base_url();?>jabatan/listjabatan">Jabatan</a></li>
        <li class="active">Detail Jabatan</li>
      </ol>
    </section>
    <section class="content">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Jabatan</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
                    <tr align="center" bgcolor="#CCCCCC">
                <td><?php

                      foreach($detailJabatan as $data){
                      $kode_jabatan		=$data->kode_jabatan;
                      $nama_jabatan=$data->nama_jabatan;
                      $keterangan=$data->keterangan;
                      }

                  ?>
                  Kode Jabatan		:<?=$kode_jabatan;?>
                  <br/>
                  Nama Jabatan	:
                  <?=$nama_jabatan;?>
                  <br/>
                  Keterangan	:
                  <?=$keterangan;?>
                  <br/></td>
              </tr>
        </div>
      </div>
      <!-- /.box -->
    </section>
</div>
