<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        TOKO JAYA ABADI
        <small>Jl. Raya Cilincing No.23</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Master</a></li>
        <li><a href="<?= base_url();?>barang/listbarang">Barang</a></li>
        <li class="active">Edit Barang</li>
      </ol>
    </section>
    <section class="content" >
      <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Barang</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                      title="Collapse">
                <i class="fa fa-minus"></i></button>
              <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
              <?php
                foreach($editBarang as $data){
                  $kode_barang		=$data->kode_barang;
                  $nama_barang=$data->nama_barang;
                  $harga_barang=$data->harga_barang;
                  $kode_jenis=$data->kode_jenis;
                  $stok=$data->stok;
                  }
              ?>
            <form class="form" action="<?=base_url()?>barang/editBarang/<?=$kode_barang;?>" method="POST" enctype="multipart/form-data" >
              <div class="box box-warning">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Jabatan</h3>
                </div>
                <!-- /.box-header -->
                  <div class="box-body">
                    <!-- text input -->
                    <div class="form-group">
                      <label>Kode Barang</label>
                      <input type="text" class="form-control" name="kode_barang" id="kode_barang" value="<?=$kode_barang;?>" readonly="on">
                    </div>
                    <div class="form-group">
                      <label>Nama Barang</label>
                      <input type="text" class="form-control" name="nama_barang" id="nama_barang" value="<?=$nama_barang;?>">
                    </div>
                    <div class="form-group">
                      <label>Jenis Barang</label>
                        <select class="form-control" style="width: 100%;" name="kode_jenis" id="kode_jenis">
                        <?php foreach($data_jenis as $data) {$select_jenis=($data->kode_jenis==$kode_jenis)?'selected':'';?>
                          <option value="<?=$data->kode_jenis;?>" <?=$select_jenis;?>>
                            <?=$data->nama_jenis;?>
                          </option>
                        <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                      <label>Stok</label>
                      <input type="text" class="form-control" name="atok" id="stok" value="<?=$stok;?>" readonly="on">
                    </div>
                    <div class="form-group">
                      <button type="reset" class="btn btn-default">Cancel</button>
                      <button type="submit" class="btn btn-info pull-right">Submit</button>
                    </div>                
              </div>
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-clock-o"></i>
                  <div align="center" style="color:red"><?=validation_errors();?></div>
                </div>           
              </div>
              <!-- /.box-body -->
            </div>
            </form>
        </div>
      </div>
      <!-- /.row -->
    </section>
</div>
