<?php defined('BASEPATH') OR exit('No direct script access allowed');

class barang_model extends CI_Model
{
    //panggil nama table
    private $_table = "barang";

    // public function tampilDataBarang()
    // {
    //     // seperti : select * from <nama_table>
    //     return $this->db->get($this->_table)->result();
    // }
	public function rules()
	{
		return[
			[
				'field' => 'kode_barang', //diambil dari name di form input
				'label' => 'Kode Barang',
				'rules'	=> 'required|max_length[5]',
				'errors'=> [
					'required'	=> 'Kode Barang tidak boleh kosong.',
					'max_length'=> 'Kode Barang tidak boleh Lebih dari 5 Karakter.',

				],
			],
			[
				'field' => 'nama_barang',
				'label' => 'Nama Barang',
				'rules'	=> 'required',
				'errors'=> [
					'required'	=> 'Nama Barang tidak boleh kosong.',
				],
			],
			
			[
				'field' => 'harga_barang',
				'label' => 'Harga Barang',
				'rules'	=> 'required|numeric',
				'errors'=> [
					'required'	=> 'Harga Barang tidak boleh kosong.',
					'numeric'	=> 'Harga Barang Harus Angka.',
				],
			
			],
			
			[
				'field' => 'kode_jenis',
				'label' => 'Nama Jenis',
				'rules'	=> 'required',
				'errors'=> [
					'required'	=> 'Kode Jenis tidak boleh kosong.',
				],
			
			],
			
			[
				'field' => 'stok',
				'label' => 'Stok Barang',
				'rules'	=> 'required|numeric',
				'errors'=> [
					'required'	=> 'Stok tidak boleh kosong.',
					'numeric'	=> 'Stok Harus Angka.',
				],	
			]
		];						
	}

	public function rules_kodeBarang()
	{
		return[
			[
				'field' => 'kode_barang', //diambil dari name di form input
				'label' => 'Kode Barang',
				'rules'	=> 'trim|required|max_length[5]|is_unique[barang.kode_barang]',
				'errors'=> [
					'required'	=> 'Kode Barang tidak boleh kosong.',
					'max_length'=> 'Kode Barang tidak boleh Lebih dari 5 Karakter.',
					'is_unique'	=> 'Kode Barang tidak boleh Sama !!..',
				],
			],
			[
				'field' => 'nama_barang',
				'label' => 'Nama Barang',
				'rules'	=> 'required',
				'errors'=> [
					'required'	=> 'Nama Barang tidak boleh kosong.',
				],
			],
			
			[
				'field' => 'harga_barang',
				'label' => 'Harga Barang',
				'rules'	=> 'required|numeric',
				'errors'=> [
					'required'	=> 'Harga Barang tidak boleh kosong.',
					'numeric'	=> 'Harga Barang Harus Angka.',
				],
			
			],
			
			[
				'field' => 'kode_jenis',
				'label' => 'Nama Jenis',
				'rules'	=> 'required',
				'errors'=> [
					'required'	=> 'Kode Jenis tidak boleh kosong.',
				],
			
			],
			
			[
				'field' => 'stok',
				'label' => 'Stok Barang',
				'rules'	=> 'required|numeric',
				'errors'=> [
					'required'	=> 'Stok tidak boleh kosong.',
					'numeric'	=> 'Stok Harus Angka.',
				],	
			]
		];						
	}
	
    public function tampilDataBarang()
    {
        $query	= $this->db->query(
            "SELECT barang.*, 
                jenis_barang.nama_jenis 
                FROM barang INNER JOIN  jenis_barang 
                ON barang.kode_jenis = jenis_barang.kode_jenis 
                WHERE barang.flag = 1"
        );
        return $query->result();	
	}
    
    public function detail($kode_barang)
    {
        $query	= $this->db->query("SELECT barang.*, jenis_barang.nama_jenis FROM barang INNER JOIN  jenis_barang ON barang.kode_jenis = jenis_barang.kode_jenis WHERE barang.flag = 1 AND barang.kode_barang = '$kode_barang'");
        return $query->result();
    }

    public function tampilDataBarang2()
    {
        $query	= $this->db->query("SELECT * FROM barang WHERE flag = 1");
        return $query->result();	
    }

    public function detail2($kode_barang)
    {
        $query	= $this->db->query("SELECT * FROM barang WHERE flag = 1 AND kode_barang = '$kode_barang'");
        return $query->result();	
    }

    public function save()
    {
		$kode_barang = $this->input->POST('kode_barang');
		$sql = $this->db->query("SELECT kode_barang FROM barang where kode_barang='$kode_barang'");
		$cek_kodeBarang = $sql->num_rows();
			// echo "<prev>";
			// print_r($sql);die();
			// echo "</prev>";	
			if ($cek_kodeBarang > 0) 
				{
					$this->session->set_flashdata();
					redirect('barang/inputBarang');
				}else
				{
        			$data['kode_barang']   = $this->input->post('kode_barang');
        			$data['nama_barang']   = $this->input->post('nama_barang');
       				$data['harga_barang']     = $this->input->post('harga_barang');
       				$data['kode_jenis']     = $this->input->post('nama_jenis');
        			$data['stok']           = $this->input->post('stok');
        			$data['flag']           = 1;
					$this->db->insert($this->_table, $data);
				}
    }

    public function update($kode_barang)
    {
        			$data['kode_barang']   = $this->input->post('kode_barang');
        			$data['nama_barang']   = $this->input->post('nama_barang');
					$data['harga_barang']  = $this->input->post('harga_barang');
					$data['kode_jenis']    = $this->input->post('kode_jenis');
        			$data['flag']          = 1;
        			$this->db->where('kode_barang', $kode_barang);
					$this->db->update($this->_table, $data);
				
    }

    public function delete($kode_barang)
    {
        //soft delete
        // $data['flag']           = 0;        
        // $this->db->where('kode_barang', $kode_barang);
        // $this->db->update($this->_table, $data);

        //delete from db
        $this->db->where('kode_barang', $kode_barang);
        $this->db->delete($this->_table);
    }

    public function updateStok($kd_barang, $qty)
    {
        //panggil data detail stok
        $cari_stok = $this->detail($kd_barang);
        foreach ($cari_stok as $data) {
            $stok = $data->stok;
        }
        
        //proses update stok table barang
        $jumlah_stok     = $stok + $qty;
        $data_barang['stok']    = $jumlah_stok;
        
        $this->db->where('kode_barang', $kd_barang);
        $this->db->update('barang', $data_barang);
	}
	public function updateStokJual($kd_barang, $qty)
    {
        //panggil data detail stok
        $cari_stok = $this->detail($kd_barang);
        foreach ($cari_stok as $data) {
            $stok = $data->stok;
        }
        
        //proses update stok table barang
        $jumlah_stok     = $stok - $qty;
        $data_barang['stok']    = $jumlah_stok;
        
        $this->db->where('kode_barang', $kd_barang);
        $this->db->update('barang', $data_barang);
    }

    public function tampilDataBarangPagination($perpage,$uri,$data_pencarian)
	{
		$this->db->select('barang.kode_barang, barang.nama_barang, barang.harga_barang, barang.harga_barang, 
		jenis_barang.nama_jenis, barang.stok');
		$this->db->join('jenis_barang', 'jenis_barang.kode_jenis=barang.kode_jenis');
		if(!empty($data_pencarian)){
			$this->db->like('barang.nama_barang',$data_pencarian);
		}

		$this->db->order_by('barang.kode_barang','asc');

		$get_data=$this->db->get($this->_table,$perpage,$uri);
		if($get_data->num_rows()>0){
			return $get_data->result();
		}else{
			return null;
		}
	}

    public function tombolpagination($data_pencarian)
	{
		//cari jumlah data berdasarkan data pencarian
		$this->db->like('nama_barang',$data_pencarian);
		$this->db->from($this->_table);
		$hasil=$this->db->count_all_results();

		// pagination limit
		$pagination['base_url']=base_url().'barang/listBarang/load/';
		$pagination['total_rows']=$hasil;
		$pagination['per_page']="3";
		$pagination['uri_segment']=4;
		$pagination['num_links']=2;

		//echo

		//custom paging configuration

		$pagination['full_tag_open']='<div class="pagination">';
		$pagination['full_tag_close']='</div>';

		$pagination['first_link']='First Page';
		$pagination['first_tag_open']='<span class="firstlink">';
		$pagination['first_tag_close']='</span>';

		$pagination['last_link']='Last Page';
		$pagination['last_tag_open']='<span class="lastlink">';
		$pagination['last_tag_close']='</span>';

		$pagination['next_link']='Next Page';
		$pagination['next_tag_open']='<span class="nextlink">';
		$pagination['next_tag_close']='</span>';

		$pagination['prev_link']='Prev Page';
		$pagination['prev_tag_open']='<span class="prevlink">';
		$pagination['prev_tag_close']='</span>';

		$this->pagination->initialize($pagination);
		$hasil_pagination=$this->tampilDataBarangPagination($pagination['per_page'],
		$this->uri->segment(4),$data_pencarian);

		return $hasil_pagination;
	}

	public function createKodeUrut()
	{
		//cek kode barang terakhir
		$this->db->select('MAX(kode_barang) as kode_barang');
		$query	=$this->db->get($this->_table);
		$result	=$query->row_array(); //hasil berbentuk array

		$kode_barang_terakhir = $result['kode_barang'];
		//format BR001= BR(label awal), 001 (nomer urut)

		$label="BR";
		$no_urut_lama	=(int) substr($kode_barang_terakhir,2,3);
		$no_urut_lama++;

		$no_urut_baru	=sprintf("%03s",$no_urut_lama);
		$kode_barang_baru=$label.$no_urut_baru;

		//var_dump($kode_barang_baru);die();
		//var_dump(sprintf("%03s",$no_urut_lama));die();

		return $kode_barang_baru;

	}

	public function cariHarga($kode_barang)
    {
        $query	= $this->db->query("SELECT * FROM barang WHERE flag = 1 AND kode_barang='$kode_barang'");
		$hasil= $query->result();
		foreach($hasil as $data)
		{
			$harganya=$data->harga_barang;
		}
		return $harganya;	
    }
}
