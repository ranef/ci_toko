<?php defined('BASEPATH') OR exit('No direct script access allowed');


class karyawan_model extends CI_Model
{
	//panggil nama table
	private $_table="karyawan";

	private function uploadPhoto($nik)
	{
		$date_upload			=date('Ymd');
		$config['upload_path']	='./resources/fotokaryawan/';
		$config['allowed_types']='gif|jpg|png|jpeg';
		$config['file_name']	=$date_upload.'-'.$nik;
		$config['overwrite']	=true;
		$config['max_size']	=1024; // 1MB
		//$config['max_width']	=1024;
		//$config['max_height']	=768;

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('image')){
			$name_file	=$this->upload->data("file_name");
		}else{
			$name_file="default.png";
		}
		return $name_file;
	}

	private function hapusFoto($nik)
	{
		//cari nama file
		$data_karyawan	=$this->detailKaryawan($nik);
		foreach ($data_karyawan as $data) {
			$nama_file	=$data->foto;
		}

		if ($nama_file != "default.png")
		{
			$path="./resources/fotokaryawan/". $nama_file;
			//bentuk path nya : .//resources/fotokaryawan/20190328_9876876.jpeg
			return unlink($path);
		}
	}
	
	public function tampilDataKaryawan()
	{
		// seperti : select * from <>nama_table>
		return $this->db->get($this->_table)->result();
	}
	
	public function tampilDataKaryawan2()
	{
		$query = $this->db->query("SELECT * FROM karyawan WHERE flag=1");
		return $query->result();
	}
	
	public function tampilDataKaryawan3()
	{
		$this->db->select('*');
		$this->db->order_by('nik','ASC');
		$result=$this->db->get($this->_table);
		return $result->result();
	}	
	
	public function saveDataKaryawan()
	{
		$nik = $this->input->POST('nik');
		$sql = $this->db->query("SELECT nik FROM karyawan where nik='$nik'");
		$cek_nik = $sql->num_rows();
			// echo "<prev>";
			// print_r($sql);die();
			// echo "</prev>";	
			if ($cek_nik > 0) 
				{
					$this->session->set_flashdata();
					redirect('karyawan/inputkaryawan');
				}else
				{					
					$nik_karyawan=$this->input->post('nik');
		
					$data['nik']			=$this->input->post('nik');
					$data['nama_lengkap']	=$this->input->post('nama_karyawan');
					$data['tempat_lahir']	=$this->input->post('tempat_lahir');
					$data['tgl_lahir']		=$this->input->post('tanggal');
					$data['jenis_kelamin']	=$this->input->post('jenis_kelamin');
					$data['alamat']			=$this->input->post('alamat');
					$data['telp']			=$this->input->post('telp');
					$data['kode_jabatan']	=$this->input->post('nama_jabatan');
					$data['flag']			=1;
					$data['foto']			=$this->uploadPhoto($nik);
					$this->db->insert($this->_table,$data);
				}
				// echo "<prev>";
            	// print_r($nik_karyawan);die();
            	// echo "</prev>";
	}
		
	public function detailKaryawan($nik)
	{
		$this->db->select('*');
		$this->db->where('nik',$nik);
		$this->db->where('flag',1);
		$result=$this->db->get($this->_table);
		return $result->result();
	}

	public function editKaryawan($nik)
	{

		$this->db->select('*');
		$this->db->where('nik',$nik);
		$this->db->where('flag',1);
		$result=$this->db->get($this->_table);
		return $result->result();
	}

	public function updateKaryawan($nik)
	{		
		
		$data['nama_lengkap']	=$this->input->post('nama_karyawan');
		$data['tempat_lahir']	=$this->input->post('tempat_lahir');
		$data['tgl_lahir']		=$this->input->post('tanggal');
		$data['jenis_kelamin']	=$this->input->post('jenis_kelamin');
		$data['alamat']			=$this->input->post('alamat');
		$data['telp']			=$this->input->post('telp');
		$data['kode_jabatan']	=$this->input->post('nama_jabatan');
		$data['flag']			=1;

		if (!empty($_FILES["image"]["name"])){
			$this->hapusFoto($nik);
			$foto_karyawan	=$this->uploadPhoto($nik);
		}else{
			$foto_karyawan	=$this->input->post('foto_old');
		}
		$data['foto']	=$foto_karyawan;

		$this->db->where('nik',$nik);
		$this->db->update($this->_table,$data);
	}


	public function delete($nik)
	{
		$this->hapusFoto($nik);	
		$this->db->where('nik',$nik);
		$this->db->delete($this->_table);
	}

	public function tampilDataKaryawanPagination($perpage,$uri,$data_pencarian)
	{
		$this->db->select('*');
		if(!empty($data_pencarian)){
			$this->db->like('nama_lengkap',$data_pencarian);
		}

		$this->db->order_by('nik','asc');

		$get_data=$this->db->get($this->_table,$perpage,$uri);
		if($get_data->num_rows()>0){
			return $get_data->result();
		}else{
			return null;
		}
	}

	public function tombolpagination($data_pencarian)
	{
		//cari jumlah data berdasarkan data pencarian
		$this->db->like('nama_lengkap',$data_pencarian);
		$this->db->from($this->_table);
		$hasil=$this->db->count_all_results();

		// pagination limit
		$pagination['base_url']=base_url().'karyawan/listkaryawan/load/';
		$pagination['total_rows']=$hasil;
		$pagination['per_page']="3";
		$pagination['uri_segment']=4;
		$pagination['num_links']=2;

		//echo

		//custom paging configuration

		$pagination['full_tag_open']='<div class="pagination">';
		$pagination['full_tag_close']='</div>';

		$pagination['first_link']='First Page';
		$pagination['first_tag_open']='<span class="firstlink">';
		$pagination['first_tag_close']='</span>';

		$pagination['last_link']='Last Page';
		$pagination['last_tag_open']='<span class="lastlink">';
		$pagination['last_tag_close']='</span>';

		$pagination['next_link']='Next Page';
		$pagination['next_tag_open']='<span class="nextlink">';
		$pagination['next_tag_close']='</span>';

		$pagination['prev_link']='Prev Page';
		$pagination['prev_tag_open']='<span class="prevlink">';
		$pagination['prev_tag_close']='</span>';

		$this->pagination->initialize($pagination);
		$hasil_pagination=$this->tampilDataKaryawanPagination($pagination['per_page'],
		$this->uri->segment(4),$data_pencarian);

		return $hasil_pagination;
	}

	public function rules()
	{
		return[
			[
				'field' => 'nik', //diambil dari name di form input
				'label' => 'NIK',
				'rules'	=> 'required|max_length[10]|numeric',
				'errors'=> [
					'required'	=> 'NIK tidak boleh kosong.',
					'max_length'=> 'NIK tidak boleh Lebih dari 10 Karakter.',
					'numeric'	=> 'NIK hanya boleh Angka.',
				],
			],
			[
				'field' => 'nama_karyawan',
				'label' => 'Nama Karyawan',
				'rules'	=> 'required',
				'errors'=> [
					'required'	=> 'Nama Karyawan tidak boleh kosong.',
				],
			],
			
			[
				'field' => 'tempat_lahir',
				'label' => 'Tempat Lahir',
				'rules'	=> 'required',
				'errors'=> [
					'required'	=> 'Tempat Lahir tidak boleh kosong.',
				],
			
			],
			
			[
				'field' => 'telp',
				'label' => 'Telp',
				'rules'	=> 'required|numeric',
				'errors'=> [
					'required'	=> 'No Telp. tidak boleh kosong.',
					'numeric'	=> 'No Telp. tidak boleh selain angka.',
				],
			
			],
			
			[
				'field' => 'alamat',
				'label' => 'Alamat',
				'rules'	=> 'required',
				'errors'=> [
					'required'	=> 'Alamat tidak boleh kosong.',
				],	
			]
		];						
	}

	public function rules_nik()
	{
		return[
			[
				'field' => 'nik', //diambil dari name di form input
				'label' => 'NIK',
				'rules'	=> 'trim|required|max_length[10]|numeric|is_unique[karyawan.nik]',
				'errors'=> [
					'required'	=> 'NIK tidak boleh kosong.',
					'max_length'=> 'NIK tidak boleh Lebih dari 10 Karakter.',
					'numeric'	=> 'NIK hanya boleh Angka.',
					'is_unique'	=> 'NIK tidak boleh Sama !!..',
				],
			],
			[
				'field' => 'nama_karyawan',
				'label' => 'Nama Karyawan',
				'rules'	=> 'required',
				'errors'=> [
					'required'	=> 'Nama Karyawan tidak boleh kosong.',
				],
			],
			
			[
				'field' => 'tempat_lahir',
				'label' => 'Tempat Lahir',
				'rules'	=> 'required',
				'errors'=> [
					'required'	=> 'Tempat Lahir tidak boleh kosong.',
				],
			
			],
			
			[
				'field' => 'telp',
				'label' => 'Telp',
				'rules'	=> 'required|numeric',
				'errors'=> [
					'required'	=> 'No Telp. tidak boleh kosong.',
					'numeric'	=> 'No Telp. tidak boleh selain angka.',
				],
			
			],
			
			[
				'field' => 'alamat',
				'label' => 'Alamat',
				'rules'	=> 'required',
				'errors'=> [
					'required'	=> 'Alamat tidak boleh kosong.',
				],	
			]
		];						
	}
	
	public function createKodeUrut()
	{
		date_default_timezone_set('Asia/Jakarta'); //set jam sesuai jakarta;
		//cek kode barang terakhir
		$this->db->select('MAX(nik) as nik');
		$query	=$this->db->get($this->_table);
		$result	=$query->row_array(); //hasil berbentuk array

		$nik_terakhir = $result['nik'];
		//format BR001= BR(label awal), 001 (nomer urut)
		$thn_sekarang=date('y');
		$bln_sekarang=date('m');
		$no_urut_lama	=(int) substr($nik_terakhir,4,3);
		$no_urut_lama++;

		$no_urut_baru	=sprintf("%03s",$no_urut_lama);
		$kode_nik_baru=$thn_sekarang.$bln_sekarang.$no_urut_baru;

		//var_dump($kode_nik_baru);die();
		//var_dump(sprintf("%03s",$no_urut_lama));die();

		return $kode_nik_baru;

	}

}