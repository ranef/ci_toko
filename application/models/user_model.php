<?php defined('BASEPATH') OR exit('No direct script access allowed');


class user_model extends CI_Model
{
	//panggil nama table
	private $_table="user";
	
	public function cekUser($username, $password)
	{

		//untuk cek kesamaan database
		$this->db->select('nik, email, tipe');
		$this->db->where('nik', $username);
		$this->db->where('password', md5($password));
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);

		//hasil ada langsung berbentuk array dan tidak perlu foreach
		return $result->row_array();
	}

	public function input_user()
	{
		
		$data['nama']   =    $this->input->post('name');
        $data['username'] =  $this->input->post('username');
        $data['email']  =    "user@gmail.com";
        $data['password'] =    md5($this->input->post('password'));
 
    	$this->m_account->daftar($data);
		$this->session->set_flashdata('info', '<div style="color: green">Simpan Data Berhasil !</div>');
		redirect("auth/registerr", "refresh");
		
		
		$this->load->view('v_register',$data);
	}

}