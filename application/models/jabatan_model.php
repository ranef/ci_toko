<?php defined('BASEPATH') OR exit('No direct script access allowed');


class jabatan_model extends CI_Model
{
	//panggil nama table
	private $_table="jabatan";
	
	public function tampilDataJabatan()
	{
		// seperti : select * from <>nama_table>
		return $this->db->get($this->_table)->result();
	}
	
	public function detailJabatan($kode_jabatan)
	{
		$this->db->select('*');
		$this->db->where('kode_jabatan',$kode_jabatan);
		$this->db->where('flag',1);
		$result=$this->db->get($this->_table);
		return $result->result();
	}
	
	public function saveDataJabatan()
	{	
		$kode_jabatan = $this->input->POST('kode_jabatan');
		$sql = $this->db->query("SELECT kode_jabatan FROM jabatan where kode_jabatan='$kode_jabatan'");
		$cek_kodeJabatan = $sql->num_rows();
			// echo "<prev>";
			// print_r($sql);die();
			// echo "</prev>";	
			if ($cek_kodeJabatan > 0) 
				{
					$this->session->set_flashdata();
					redirect('jabatan/inputJabatan');
				}else
				{
					$data['kode_jabatan']	=$this->input->post('kode_jabatan');
					$data['nama_jabatan']	=$this->input->post('nama_jabatan');
					$data['keterangan']	=$this->input->post('keterangan');
					$data['flag']	=1;
					$this->db->insert($this->_table,$data);
				}
	}

	public function editJabatan($kode_jabatan)
	{
		$this->db->select('*');
		$this->db->where('kode_jabatan',$kode_jabatan);
		$this->db->where('flag',1);
		$result=$this->db->get($this->_table);
		return $result->result();
	}

	public function updateJabatan($kode_jabatan)
	{
		$data['kode_jabatan']	=$this->input->post('nama_jabatan');
		$data['keterangan']	=$this->input->post('keterangan');
		$data['flag']	=1;

		$this->db->where('kode_jabatan',$kode_jabatan);
		$this->db->update($this->_table,$data);
	}

	public function delete($kode_jabatan)
	{
		$this->db->where('kode_jabatan',$kode_jabatan);
		$this->db->delete($this->_table);
	}

	public function rules()
	{
		return[
			[
				'field' => 'kode_jabatan', //diambil dari name di form input
				'label' => 'Kode Jabatan',
				'rules'	=> 'required|max_length[5]',
				'errors'=> [
					'required'	=> 'Kode Jabatan tidak boleh kosong.',
					'max_length'=> 'Kode Jabatan tidak boleh Lebih dari 5 Karakter.',

				],
			],
			[
				'field' => 'nama_jabatan',
				'label' => 'Nama Jabatan',
				'rules'	=> 'required',
				'errors'=> [
					'required'	=> 'Nama Jabatan tidak boleh kosong.',
				],
			],
			
			[
				'field' => 'keterangan',
				'label' => 'keterangan',
				'rules'	=> 'required',
				'errors'=> [
					'required'	=> 'Keterangan tidak boleh kosong.',
				],	
			]
		];						
	}

	public function tampilDataJabatanPagination($perpage,$uri,$data_pencarian)
	{
		$this->db->select('*');
		if(!empty($data_pencarian)){
			$this->db->like('nama_jabatan',$data_pencarian);
		}

		$this->db->order_by('kode_jabatan','asc');

		$get_data=$this->db->get($this->_table,$perpage,$uri);
		if($get_data->num_rows()>0){
			return $get_data->result();
		}else{
			return null;
		}
	}

	public function tombolpagination($data_pencarian)
	{
		//cari jumlah data berdasarkan data pencarian
		$this->db->like('nama_jabatan',$data_pencarian);
		$this->db->from($this->_table);
		$hasil=$this->db->count_all_results();

		// pagination limit
		$pagination['base_url']=base_url().'jabatan/listJabatan/load/';
		$pagination['total_rows']=$hasil;
		$pagination['per_page']="3";
		$pagination['uri_segment']=4;
		$pagination['num_links']=2;

		//echo

		//custom paging configuration

		$pagination['full_tag_open']='<div class="pagination">';
		$pagination['full_tag_close']='</div>';

		$pagination['first_link']='First Page';
		$pagination['first_tag_open']='<span class="firstlink">';
		$pagination['first_tag_close']='</span>';

		$pagination['last_link']='Last Page';
		$pagination['last_tag_open']='<span class="lastlink">';
		$pagination['last_tag_close']='</span>';

		$pagination['next_link']='Next Page';
		$pagination['next_tag_open']='<span class="nextlink">';
		$pagination['next_tag_close']='</span>';

		$pagination['prev_link']='Prev Page';
		$pagination['prev_tag_open']='<span class="prevlink">';
		$pagination['prev_tag_close']='</span>';

		$this->pagination->initialize($pagination);
		$hasil_pagination=$this->tampilDataJabatanPagination($pagination['per_page'],
		$this->uri->segment(4),$data_pencarian);

		return $hasil_pagination;
	}

	public function rules_kodeJabatan()
	{
		return[
			[
				'field' => 'kode_jabatan', //diambil dari name di form input
				'label' => 'Kode Jabatan',
				'rules'	=> 'trim|required|max_length[5]|is_unique[jabatan.kode_jabatan]',
				'errors'=> [
					'required'	=> 'Kode Jabatan tidak boleh kosong.',
					'max_length'=> 'Kode Jabatan tidak boleh Lebih dari 5 Karakter.',
					'is_unique'	=> 'Kode Jabatan tidak boleh Sama !!..',
				],
			],
			[
				'field' => 'nama_jabatan',
				'label' => 'Nama Jabatan',
				'rules'	=> 'required',
				'errors'=> [
					'required'	=> 'Nama Jabatan tidak boleh kosong.',
				],
			],
			
			[
				'field' => 'keterangan',
				'label' => 'keterangan',
				'rules'	=> 'required',
				'errors'=> [
					'required'	=> 'Keterangan tidak boleh kosong.',
				],	
			]
		];						
	}

	public function createKodeUrut()
	{
		//cek kode barang terakhir
		$this->db->select('MAX(kode_jabatan) as kode_jabatan');
		$query	=$this->db->get($this->_table);
		$result	=$query->row_array(); //hasil berbentuk array

		$kode_jabatan_terakhir = $result['kode_jabatan'];
		//format BR001= BR(label awal), 001 (nomer urut)

		$label="JB";
		$no_urut_lama	=(int) substr($kode_jabatan_terakhir,2,3);
		$no_urut_lama++;

		$no_urut_baru	=sprintf("%03s",$no_urut_lama);
		$kode_jabatan_baru=$label.$no_urut_baru;

		//var_dump($kode_barang_baru);die();
		//var_dump(sprintf("%03s",$no_urut_lama));die();

		return $kode_jabatan_baru;

	}

}
