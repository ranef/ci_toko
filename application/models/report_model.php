<?php defined('BASEPATH') OR exit('No direct script access allowed');

class report_model extends CI_Model
{
    //panggil nama table
    private $_table_header = "pembelian_header";
    private $_table_detail = "pembelian_detail";

    public function tampilDataReport()
    {
        $query	= $this->db->query("select * from pembelian_header");
        return $query->result();	
    }

    public function tampilDataReport1()
    {
        $query	= $this->db->query("select * from pembelian_header where tanggal between '.$tanggal_awal.' and '.$tanggal_akhir.'");
        return $query->result();	
    }

    public function saveReportHeader($tanggal_awal,$tanggal_akhir)
    {      

        // SELECT ph.id_pembelian_h,ph.no_transaksi,ph.tanggal,count(pd.kode_barang)as kode_barang,sum(pd.qty), sum(pd.jumlah)as qty from pembelian_header as ph INNER JOIN pembelian_detail as pd on ph.id_pembelian_h=pd.id_pembelian_h GROUP BY ph.no_transaksi ASC
            $this->db->select("ph.id_pembelian_h, ph.no_transaksi, ph.tanggal, count(pd.kode_barang) as total_barang, sum(pd.qty) as total_qty, sum(pd.jumlah) as total_pembelian, pd.harga");
            $this->db->from("pembelian_header ph");
            $this->db->join("pembelian_detail pd", "pd.id_pembelian_h=ph.id_pembelian_h");
            $this->db->where("ph.tanggal BETWEEN '$tanggal_awal' AND '$tanggal_akhir'");
            $this->db->group_by("ph.id_pembelian_h");
            $query = $this->db->get();
            
            // echo "<prev>";
            // print_r($tanggal_awal);die();
            // echo "</prev>";
            
            return $query->result();

       
    }


}
