<?php defined('BASEPATH') OR exit('No direct script access allowed');


class supplier_model extends CI_Model
{
	//panggil nama table
	private $_table="supplier";
	
	public function tampilDataSupplier()
	{
		// seperti : select * from <>nama_table>
		return $this->db->get($this->_table)->result();
	}
	
	public function tampilDataSupplier2()
	{
		$query = $this->db->query("SELECT * FROM supplier WHERE flag=1");
		return $query->result();
	}
	
	public function tampilDataSupplier3()
	{
		$this->db->select('*');
		$this->db->order_by('kode_supplier','ASC');
		$result=$this->db->get($this->_table);
		return $result->result();
	}
	
	public function detailSupplier($kode_supplier)
	{
		$this->db->select('*');
		$this->db->where('kode_supplier',$kode_supplier);
		$this->db->where('flag',1);
		$result=$this->db->get($this->_table);
		return $result->result();
	}
	
	public function saveDataSupplier()
	{	
		$kode_supplier = $this->input->POST('kode_supplier');
		$sql = $this->db->query("SELECT kode_supplier FROM supplier where kode_supplier='$kode_supplier'");
		$cek_kodeSupplier = $sql->num_rows();
			// echo "<prev>";
			// print_r($sql);die();
			// echo "</prev>";	
			if ($cek_kodeSupplier > 0) 
				{
					$this->session->set_flashdata();
					redirect('supplier/inputSupplier');
				}else
				{
					$data['kode_supplier']	=$this->input->post('kode_supplier');
					$data['nama_supplier']	=$this->input->post('nama_supplier');
					$data['alamat']	=$this->input->post('alamat');
					$data['telp']	=$this->input->post('telp');
					$data['flag']	=1;
					$this->db->insert($this->_table,$data);
				}
	}

	public function editSupplier($kode_supplier)
	{

		$this->db->select('*');
		$this->db->where('kode_supplier',$kode_supplier);
		$this->db->where('flag',1);
		$result=$this->db->get($this->_table);
		return $result->result();
	}

	public function updateSupplier($kode_supplier)
	{
		
		$data['nama_supplier']	=$this->input->post('nama_supplier');
		$data['alamat']	=$this->input->post('alamat');
		$data['telp']	=$this->input->post('telp');
		$data['flag']	=1;

		$this->db->where('kode_supplier',$kode_supplier);
		$this->db->update($this->_table,$data);
	}


	public function delete($kode_supplier)
	{
		$this->db->where('kode_supplier',$kode_supplier);
		$this->db->delete($this->_table);
	}

	public function rules_kodeSupplier()
	{
		return[
			[
				'field' => 'kode_supplier', //diambil dari name di form input
				'label' => 'Kode Supplier',
				'rules'	=> 'trim|required|max_length[5]|is_unique[supplier.kode_supplier]',
				'errors'=> [
					'required'	=> 'Kode Supplier tidak boleh kosong.',
					'max_length'=> 'Kode Supplier tidak boleh Lebih dari 5 Karakter.',
					'is_unique'	=> 'Kode Supplier tidak boleh Sama !!..',
				],
			],
			[
				'field' => 'nama_supplier',
				'label' => 'Nama Supplier',
				'rules'	=> 'required',
				'errors'=> [
					'required'	=> 'Nama Supplier tidak boleh kosong.',
				],
			],
			
			[
				'field' => 'alamat',
				'label' => 'Alamat',
				'rules'	=> 'required',
				'errors'=> [
					'required'	=> 'Alamat tidak boleh kosong.',
				],	
			],

			[
				'field' => 'telp',
				'label' => 'Telp',
				'rules'	=> 'required|numeric|max_length[15]',
				'errors'=> [
					'required'	=> 'Telp tidak boleh kosong.',
					'numeric'	=> 'Telp harus Angka.',
					'max_length'=> 'Kode Supplier tidak boleh Lebih dari 15 Karakter.',
				],	
			]
		];						
	}

	public function rules()
	{
		return[
			[
				'field' => 'kode_supplier', //diambil dari name di form input
				'label' => 'Kode Supplier',
				'rules'	=> 'required|max_length[5]',
				'errors'=> [
					'required'	=> 'Kode Supplier tidak boleh kosong.',
					'max_length'=> 'Kode Supplier tidak boleh Lebih dari 5 Karakter.',

				],
			],
			[
				'field' => 'nama_supplier',
				'label' => 'Nama Supplier',
				'rules'	=> 'required',
				'errors'=> [
					'required'	=> 'Nama Supplier tidak boleh kosong.',
				],
			],
			
			[
				'field' => 'alamat',
				'label' => 'Alamat',
				'rules'	=> 'required',
				'errors'=> [
					'required'	=> 'Alamat tidak boleh kosong.',
				],	
			],

			[
				'field' => 'telp',
				'label' => 'Telp',
				'rules'	=> 'required|numeric|max_length[15]',
				'errors'=> [
					'required'	=> 'Telp tidak boleh kosong.',
					'numeric'	=> 'Telp harus Angka.',
					'max_length'=> 'Kode Supplier tidak boleh Lebih dari 15 Karakter.',
				],	
			]
		];						
	}

	public function tampilDataSupplierPagination($perpage,$uri,$data_pencarian)
	{
		$this->db->select('*');
		if(!empty($data_pencarian)){
			$this->db->like('nama_supplier',$data_pencarian);
		}

		$this->db->order_by('kode_supplier','asc');

		$get_data=$this->db->get($this->_table,$perpage,$uri);
		if($get_data->num_rows()>0){
			return $get_data->result();
		}else{
			return null;
		}
	}

	public function tombolpagination($data_pencarian)
	{
		//cari jumlah data berdasarkan data pencarian
		$this->db->like('nama_supplier',$data_pencarian);
		$this->db->from($this->_table);
		$hasil=$this->db->count_all_results();

		// pagination limit
		$pagination['base_url']=base_url().'supplier/listSupplier/load/';
		$pagination['total_rows']=$hasil;
		$pagination['per_page']="3";
		$pagination['uri_segment']=4;
		$pagination['num_links']=2;

		//echo

		//custom paging configuration

		$pagination['full_tag_open']='<div class="pagination">';
		$pagination['full_tag_close']='</div>';

		$pagination['first_link']='First Page';
		$pagination['first_tag_open']='<span class="firstlink">';
		$pagination['first_tag_close']='</span>';

		$pagination['last_link']='Last Page';
		$pagination['last_tag_open']='<span class="lastlink">';
		$pagination['last_tag_close']='</span>';

		$pagination['next_link']='Next Page';
		$pagination['next_tag_open']='<span class="nextlink">';
		$pagination['next_tag_close']='</span>';

		$pagination['prev_link']='Prev Page';
		$pagination['prev_tag_open']='<span class="prevlink">';
		$pagination['prev_tag_close']='</span>';

		$this->pagination->initialize($pagination);
		$hasil_pagination=$this->tampilDataSupplierPagination($pagination['per_page'],
		$this->uri->segment(4),$data_pencarian);

		return $hasil_pagination;
	}

	public function createKodeUrut()
	{
		date_default_timezone_set('Asia/Jakarta'); //set jam sesuai jakarta;
		//cek kode barang terakhir
		$this->db->select('MAX(kode_supplier) as kode_supplier');
		$query	=$this->db->get($this->_table);
		$result	=$query->row_array(); //hasil berbentuk array

		$kode_supplier_terakhir = $result['kode_supplier'];
		//format BR001= BR(label awal), 001 (nomer urut)
		$no_urut_lama	=(int) substr($kode_supplier_terakhir,3,3);
		$no_urut_lama++;

		$no_urut_baru	=sprintf("%03s",$no_urut_lama);
		$kode_supplier_baru="SP".$no_urut_baru;

		//var_dump($kode_nik_baru);die();
		//var_dump(sprintf("%03s",$no_urut_lama));die();

		return $kode_supplier_baru;
	}
}