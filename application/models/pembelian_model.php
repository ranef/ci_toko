<?php defined('BASEPATH') OR exit('No direct script access allowed');

class pembelian_model extends CI_Model
{
    //panggil nama table
    private $_table_header = "pembelian_header";
    private $_table_detail = "pembelian_detail";
	// $this->load->model("supplier_model");
    public function tampilDataPembelian()
    {
        $query	= $this->db->query(
            "SELECT * FROM " . $this->_table_header . " WHERE flag = 1"
        );
        return $query->result();	
	}
	public function tampilDataPembelian1()
    {
        $$query	= $this->db->query(
            "SELECT " . $this->_table_header . ".*, 
                supplier.nama_supplier 
                FROM " . $this->_table_header . " INNER JOIN  supplier
                ON " . $this->_table_header . ".kode_supplier = supplier.kode_supplier 
                WHERE barang.flag = 1"
        );
        return $query->result();	
    }

    public function savePembelianHeader()
    {
        $data['no_transaksi']   = $this->input->post('no_transaksi');
        $data['kode_supplier']  = $this->input->post('kode_supplier');
        $data['tanggal']        = date('Y-m-d');
        $data['approved']       = 1;
        $data['flag']           = 1;

        $this->db->insert($this->_table_header, $data);
    }

    public function idTransaksiTerakhir()
    {
        $query	= $this->db->query(
            "SELECT * FROM " . $this->_table_header . " WHERE flag = 1 ORDER BY id_pembelian_h DESC LIMIT 0,1"
        );
        $data_id = $query->result();

        foreach ($data_id as $data) {
            $last_id = $data->id_pembelian_h;
        }

        return $last_id;
    }

    public function tampilDataPembelianDetail($id)
    {
        $query	= $this->db->query(
            "SELECT A.*, B.nama_barang FROM " . $this->_table_detail . " AS A INNER JOIN barang AS B ON A.kode_barang = B.kode_barang WHERE A.flag = 1 AND A.id_pembelian_h = '$id'"
        );
        return $query->result();	
    }

    public function savePembelianDetail($id)
    {
       
					$qty    = $this->input->post('qty');
					$kode_barang=$this->input->post('kode_barang');
                    $hargamenu  = $this->barang_model->cariHarga($kode_barang);

                    $data['id_pembelian_h'] = $id;
                    $data['kode_barang']    =$kode_barang;
                    $data['qty']            = $qty;
					$data['harga']          = $hargamenu;
                    $data['jumlah']         = $qty * $hargamenu;
                    $data['flag']           = 1;

                    $this->db->insert($this->_table_detail, $data);
                
    }

    public function rules()
	{
		return[
			[
				'field' => 'no_transaksi', //diambil dari name di form input
				'label' => 'No Transaksi',
				'rules'	=> 'trim|required|max_length[10]|is_unique[pembelian_header.no_transaksi]',
				'errors'=> [
					'required'	=> 'No Transaksi tidak boleh kosong.',
                    'max_length'=> 'No Transaksi tidak boleh Lebih dari 10 Karakter.',
                    'is_unique'	=> 'Nomer Transaksi tidak boleh Sama !!..',
                ],
            ]
        ];						
    }

    public function tampilDataPembelianPagination($perpage,$uri,$data_pencarian)
	{
		$this->db->select('*');
		if(!empty($data_pencarian)){
			$this->db->like('id_pembelian_h',$data_pencarian);
		}

		$this->db->order_by('kode_supplier','asc');

		$get_data=$this->db->get($this->_table_header,$perpage,$uri);
		if($get_data->num_rows()>0){
			return $get_data->result();
		}else{
			return null;
		}
	}

	public function tombolpagination($data_pencarian)
	{
		//cari jumlah data berdasarkan data pencarian
		$this->db->like('id_pembelian_h',$data_pencarian);
		$this->db->from($this->_table_header);
		$hasil=$this->db->count_all_results();

		// pagination limit
		$pagination['base_url']=base_url().'pembelian/listPembelian/load/';
		$pagination['total_rows']=$hasil;
		$pagination['per_page']="3";
		$pagination['uri_segment']=4;
		$pagination['num_links']=2;

		//echo

		//custom paging configuration

		$pagination['full_tag_open']='<div class="pagination">';
		$pagination['full_tag_close']='</div>';

		$pagination['first_link']='First Page';
		$pagination['first_tag_open']='<span class="firstlink">';
		$pagination['first_tag_close']='</span>';

		$pagination['last_link']='Last Page';
		$pagination['last_tag_open']='<span class="lastlink">';
		$pagination['last_tag_close']='</span>';

		$pagination['next_link']='Next Page';
		$pagination['next_tag_open']='<span class="nextlink">';
		$pagination['next_tag_close']='</span>';

		$pagination['prev_link']='Prev Page';
		$pagination['prev_tag_open']='<span class="prevlink">';
		$pagination['prev_tag_close']='</span>';

		$this->pagination->initialize($pagination);
		$hasil_pagination=$this->tampilDataPembelianPagination($pagination['per_page'],
		$this->uri->segment(4),$data_pencarian);

		return $hasil_pagination;
	}

    
    public function rules1()
	{
		return[
			[
				'field' => 'qty',
				'label' => 'Qty',
				'rules'	=> 'required|numeric',
				'errors'=> [
					'required'	=> 'Quantity tidak boleh kosong.',
					'numeric'	=> 'Quantity tidak boleh selain angka.',
				],
			
            ]
            ];						
	}

	public function createKodeUrut()
	{
		date_default_timezone_set('Asia/Jakarta'); //set jam sesuai jakarta;
		//cek kode barang terakhir
		$this->db->select('MAX(no_transaksi) as no_transaksi');
		$query	=$this->db->get($this->_table_header);
		$result	=$query->row_array(); //hasil berbentuk array
		if($result >0)
		{
			$no_transaksi_terakhir = $result['no_transaksi'];
			//format BR001= BR(label awal), 001 (nomer urut)
			$thn_sekarang=substr(date('y'),1,1);
			$bln_sekarang=date('m');
			$jam_sekarang=date('H'); // H = 24 jam, h=12 jam
			$rubah_jam="";
			if($jam_sekarang%2==0){
				$rubah_jam="A"; //jam genap
			}else{
				$rubah_jam="B"; //jam ganjil
			}
			$label="TR";
			$no_urut_lama	=(int) substr($no_transaksi_terakhir,8,2);
			$no_urut_lama++;
			$no_urut_baru	=sprintf("%02s",$no_urut_lama);
			$no_transaksi_baru=$label.$thn_sekarang.$bln_sekarang.$jam_sekarang.$rubah_jam.$no_urut_baru;

			// var_dump($no_urut_baru);die();
			// var_dump(sprintf("%02s",$no_urut_lama));die();

			return $no_transaksi_baru;
		}else
		{
			$no_transaksi_terakhir = $result['no_transaksi'];
			//format BR001= BR(label awal), 001 (nomer urut)
			$thn_sekarang=substr(date('y'),1,1);
			$bln_sekarang=date('m');
			$jam_sekarang=date('H'); // H = 24 jam, h=12 jam
			$rubah_jam="";
			if($jam_sekarang%2==0){
				$rubah_jam="A"; //jam genap
			}else{
				$rubah_jam="B"; //jam ganjil
			}
			$label="TR";
			$no_urut_lama	=(int) substr($no_transaksi_terakhir,8,2);
			$no_urut_lama++;

			$no_urut_baru	=sprintf("%02s",$no_urut_lama);
			$no_transaksi_baru=$label.$thn_sekarang.$bln_sekarang.$jam_sekarang.$rubah_jam.$no_urut_baru;

			// var_dump($no_urut_baru);die();
			// var_dump(sprintf("%02s",$no_urut_lama));die();

			return $no_transaksi_baru;
		}

	}

}
