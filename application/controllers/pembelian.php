<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class pembelian extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//load model terkait
		$this->load->model("pembelian_model");
		$this->load->model("supplier_model");
        $this->load->model("barang_model");
        $this->load->library('form_validation');

        $user_login =$this->session->userdata();

        if(count($user_login)<= 1){
        redirect("auth/index", "refresh");
        }
	}

	public function index()
	{
        
		$this->listPembelian();
    }
    
    public function listPembelian()
	{
        
		if(isset($_POST['tombol_cari'])){
			$data['kata_pencarian']=$this->input->post('cari');
			$this->session->set_userdata('session_pencarian_pembelian',$data['kata_pencarian']);
		}else{
			$data['kata_pencarian']=$this->session->userdata('session_pencarian_pembelian');
		}

        //$data['data_karyawan'] =$this->karyawan_model->tampilDataKaryawan();
        $data['data_supplier'] 	    = $this->supplier_model->tampilDataSupplier();
        $data['data_pembelian']=$this->pembelian_model->tombolpagination($data['kata_pencarian']);
		$data['content']='forms/list_pembelian';
		$this->load->view('home',$data);
    }
    
    public function input_h()
	{
        // panggil data supplier untuk kebutuhan form input
        $data['data_supplier'] 	    = $this->supplier_model->tampilDataSupplier();
        $data['no_urut_transaksi']  =$this->pembelian_model->createKodeUrut();
        $data['content'] 		    = 'forms/input_pembelian';
        $validation =$this->form_validation;
		$validation->set_rules($this->pembelian_model->rules());
        if ($validation->run()){
        // proses simpan ke pembelian header jika ada request form
        if (!empty($_REQUEST)) {
            $pembelian_header = $this->pembelian_model;
            $pembelian_header->savePembelianHeader();
            
            //panggil ID transaksi terakhir
            $id_terakhir = $pembelian_header->idTransaksiTerakhir();
            // var_dump($id_terakhir); die();
            
            // redirect("pembelian/index", "refresh");
            // redirect ke halaman input pembelian detail
			redirect("pembelian/input_d/" . $id_terakhir, "refresh");
        }}
        
		$this->load->view('home', $data);
    }
    
    public function input_d($id_pembelian_header)
	{
        // panggil data barang untuk kebutuhan form input   
        // $data['harga']=$this->barang_model->select();
        $data['data_barang'] 	= $this->barang_model->tampilDataBarang();
        $data['id_header'] 	    = $id_pembelian_header;
        $data['data_pembelian_detail'] 	= $this->pembelian_model->tampilDataPembelianDetail($id_pembelian_header);
        $validation =$this->form_validation;
        $validation->set_rules($this->pembelian_model->rules1());
        
        if ($validation->run()){
        // proses simpan ke pembelian detail jika ada request form
        if (!empty($_REQUEST)) {
            //save detail
            $this->pembelian_model->savePembelianDetail($id_pembelian_header);
            
            //proses update stok
            $qty        = $this->input->post('qty');
            $kode_barang = $this->input->POST('kode_barang');
            $this->barang_model->updateStok($kode_barang, $qty);
            $this->session->set_flashdata('info', '<div style="color: green">Simpan Data Berhasil !</div>');
            redirect("pembelian/input_d/" . $id_pembelian_header, "refresh");    
		}}
        
		$data['content'] 		= 'forms/input_pembelian_detail';
		$this->load->view('home', $data);
    }
     
}
