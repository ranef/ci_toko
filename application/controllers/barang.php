<?php defined('BASEPATH') OR exit('No direct script access allowed');


class barang extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		//load model terkait
		$this->load->model("barang_model");
		$this->load->model("jenis_barang_model");
		$this->load->library('form_validation');
		//cek sesi login
		$user_login	=$this->session->userdata();

		if(count($user_login)<= 1){
		redirect("auth/index", "refresh");
		}
		
		//load sesi validasi
		$user_login = $this->session->userdata();
		if(count($user_login)<=1){
			redirect("auth/index", "refresh");
			}
		
	}

	public function index()
	{
		$this->listBarang();
	}
	
	public function listBarang()
	{
		if(isset($_POST['tombol_cari'])){
			$data['kata_pencarian']=$this->input->post('cari');
			$this->session->set_userdata('session_pencarian_barang',$data['kata_pencarian']);
		}else{
			$data['kata_pencarian']=$this->session->userdata('session_pencarian_barang');
		}
		$data['data_barang']=$this->barang_model->tombolpagination($data['kata_pencarian']);
		$data['content']='forms/list_barang';
		$this->load->view('home',$data);
	}
	
	public function inputBarang()
	{
		$data['jenis_barang']=$this->jenis_barang_model->tampilDataJenisBarang();
		$data['no_barang_urut']=$this->barang_model->createKodeUrut();
			//if(!empty($_REQUEST)){
				//$m_barang=$this->barang_model;
				//$m_barang->save();
				//redirect("barang/index","refresh");
				//}
		$validation =$this->form_validation;
		$validation->set_rules($this->barang_model->rules_kodeBarang());
		
		if ($validation->run()){
			$this->barang_model->save();
			$this->session->set_flashdata('info', '<div style="color: green">Simpan Data Berhasil !</div>');
			redirect("barang/index", "refresh");
			}
		$data['content']	='forms/input_barang';
		$this->load->view('home',$data);	
	}
	
	public function detailBarang($kode_barang)
	{
		$data['detailBarang']=$this->barang_model->detail($kode_barang);
		$data['content']	='forms/detailBarang';
		$this->load->view('home',$data);
	}


	public function editBarang($kode_barang)
	{
		$data['data_jenis'] =$this->jenis_barang_model->tampilDataJenisBarang();
		$data['editBarang']=$this->barang_model->detail($kode_barang);
		$data['content']	='forms/editBarang';
		$validation =$this->form_validation;
		$validation->set_rules($this->barang_model->rules());
		
		if ($validation->run()){
			$this->barang_model->update($kode_barang);
			$this->session->set_flashdata('info', '<div style="color: green">Update Data Berhasil !</div>');
			redirect("barang/index", "refresh");
			}

		$this->load->view('home',$data);
	}


	public function deleteBarang($kode_barang)
	{
		$m_barang=$this->barang_model;
		$m_barang->delete($kode_barang);
		redirect("barang/index","refresh");
	}	
}