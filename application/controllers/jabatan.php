<?php defined('BASEPATH') OR exit('No direct script access allowed');


class jabatan extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		//load model terkait
		$this->load->model("jabatan_model");
		$this->load->library('form_validation');
		$user_login	=$this->session->userdata();

		if(count($user_login)<= 1){
		redirect("auth/index", "refresh");
		}
	}

	public function index()
	{
		$this->listJabatan();
	}
	
	public function listJabatan()
	{
		if(isset($_POST['tombol_cari'])){
			$data['kata_pencarian_jabatan']=$this->input->post('cari');
			$this->session->set_userdata('session_pencarian_jabaran',$data['kata_pencarian_jabatan']);
		}else{
			$data['kata_pencarian_jabatan']=$this->session->userdata('session_pencarian_jabatan');
		}

		//$data['data_jabatan'] =$this->jabatan_model->tampilDataKaryawan();
		$data['data_jabatan']=$this->jabatan_model->tombolpagination($data['kata_pencarian_jabatan']);
		$data['content']='forms/list_jabatan';
		$this->load->view('home',$data);
	}
	
	public function detailJabatan($kode_jabatan)
	{
		$data['detailJabatan']=$this->jabatan_model->detailJabatan($kode_jabatan);
		$data['content']='forms/detailJabatan';
		$this->load->view('home',$data);
	}
	
	public function inputJabatan()
	{
		$kode_jabatan = $this->input->POST('kode_jabatan');
		$data['data_jabatan']=$this->jabatan_model->tampilDataJabatan();
		$data['no_urut_jabatan']=$this->jabatan_model->createKodeUrut();
		$data['content']='forms/input_jabatan';
		$validation =$this->form_validation;
		$validation->set_rules($this->jabatan_model->rules_kodeJabatan());
		
		if ($validation->run()){
			$this->jabatan_model->saveDataJabatan();
			$this->session->set_flashdata('info', '<div style="color: green">Simpan Data Berhasil !</div>');
			redirect("jabatan/index", "refresh");
			}
		
		$this->load->view('home',$data);
	}

	public function editJabatan($kode_jabatan)
	{
		$data['data_jabatan'] =$this->jabatan_model->tampilDataJabatan($kode_jabatan);
		$data['editJabatan']=$this->jabatan_model->editJabatan($kode_jabatan);
		$data['content']='forms/editJabatan';
		$validation =$this->form_validation;
		$validation->set_rules($this->jabatan_model->rules());
		
		if ($validation->run()){
			$this->jabatan_model->updateJabatan($kode_jabatan);
			$this->session->set_flashdata('info', '<div style="color: green">Update Data Berhasil !</div>');
			redirect("jabatan/index", "refresh");
			}

		$this->load->view('home',$data);
	}	

	public function deleteJabatan($kode_jabatan)
	{
		$data['content']='forms/list_jabatan';
		$m_jabatan=$this->jabatan_model;
		$m_jabatan->delete($kode_jabatan);	
		redirect("jabatan/index","refresh");
	}
}