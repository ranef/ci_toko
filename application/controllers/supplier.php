<?php defined('BASEPATH') OR exit('No direct script access allowed');


class supplier extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		//load model terkait
		$this->load->model("supplier_model");
		$this->load->library('form_validation');
		$user_login	=$this->session->userdata();

		if(count($user_login)<= 1){
		redirect("auth/index", "refresh");
		}
	}

	public function index()
	{
		$this->listSupplier();
	}
	
	public function listSupplier()
	{
		if(isset($_POST['tombol_cari'])){
			$data['kata_pencarian']=$this->input->post('cari');
			$this->session->set_userdata('session_pencarian_supplier',$data['kata_pencarian']);
		}else{
			$data['kata_pencarian']=$this->session->userdata('session_pencarian_supplier');
		}

		//$data['data_supplier'] =$this->supplier_model->tampilDataSupplier();
		$data['data_supplier']=$this->supplier_model->tombolpagination($data['kata_pencarian']);
		$data['content']='forms/list_supplier';
		$this->load->view('home',$data);
	}
	
	public function detailSupplier($kode_supplier)
	{
		$data['detailSupplier']=$this->supplier_model->detailSupplier($kode_supplier);
		$data['content']='forms/detailSupplier';
		$this->load->view('home',$data);
	}	
	
	public function inputSupplier()
	{
		$data['data_supplier']=$this->supplier_model->tampilDataSupplier();
		$data['no_urut_kode_supplier']=$this->supplier_model->createKodeUrut();
		$data['content']='forms/input_supplier';
		$validation =$this->form_validation;
		$validation->set_rules($this->supplier_model->rules_kodeSupplier());
		
		if ($validation->run()){
			$this->supplier_model->saveDataSupplier();
			$this->session->set_flashdata('info', '<div style="color: green">Simpan Data Berhasil !</div>');
			redirect("supplier/index", "refresh");
			}
		$this->load->view('home',$data);
	}

	public function editSupplier($kode_supplier)
	{
		$data['data_supplier']=$this->supplier_model->tampilDataSupplier();
		$data['editSupplier']=$this->supplier_model->editSupplier($kode_supplier);
		$data['content']='forms/editSupplier';
		$validation =$this->form_validation;
		$validation->set_rules($this->supplier_model->rules());
		
		if ($validation->run()){
			$this->supplier_model->updateSupplier($kode_supplier);
			$this->session->set_flashdata('info', '<div style="color: green">Update Data Berhasil !</div>');
			redirect("supplier/index", "refresh");
			}
		
		$this->load->view('home',$data);
	}

	public function deleteSupplier($kode_supplier)
	{
		$m_supplier=$this->supplier_model;
		$m_supplier->delete($kode_supplier);
		redirect("supplier/index","refresh");
	}
}