<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class report extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//load model terkait
		$this->load->model("report_model");
		$this->load->library('pdf');
        $user_login =$this->session->userdata();

        if(count($user_login)<= 1){
        redirect("auth/index", "refresh");
        }
	}

	public function index()
	{
		$this->listReport();
    }
    
    public function listReport()
	{
        
        $data['content'] 		 = 'forms/report';
		$this->load->view('home', $data);
    }
   

    public function report_d()
	{

        if(!empty($_REQUEST))
        {
            $tanggal_awal    = $this->input->post('tanggal_awal');
            $tanggal_akhir    = $this->input->post('tanggal_akhir');
        
        $data['tgl_awal']	=$this->input->post('tanggal_awal');
        $data['tgl_akhir']	=$this->input->post('tanggal_akhir');
        $data['report_header'] 	= $this->report_model->saveReportHeader($tanggal_awal,$tanggal_akhir);
        $data['content'] 		= 'forms/report_detail';
        $this->load->view('home', $data);
        }else{
            redirect("report/","refresh");
        }
         
    }
    
    public function report_pen()
	{

        if(!empty($_REQUEST))
        {
            $tanggal_awal    = $this->input->post('tanggal_awal');
            $tanggal_akhir    = $this->input->post('tanggal_akhir');
        
        $data['tgl_awal']	=$this->input->post('tanggal_awal');
        $data['tgl_akhir']	=$this->input->post('tanggal_akhir');
        $data['report_header'] 	= $this->report_model->saveReportHeader($tanggal_awal,$tanggal_akhir);
        $data['content'] 		= 'forms/report_detail';
        $this->load->view('home', $data);
        }else{
            redirect("report/","refresh");
        }
         
	}
	
	// public function cariPdf()
	// {
	// 	if(!empty($_REQUEST)){
	// 		$tanggal_awal=$this->
	// 	}
	// }

    public function cetakPdf($tgl_awal,$tgl_akhir)
	{
        $report=$this->report_model->saveReportHeader($tgl_awal,$tgl_akhir);
        // echo "<prev>";
        // print_r($tanggal_awal);die();
        // echo "</prev>";
        $pdf = new FPDF('P','mm','A4');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial','B',16);
        // mencetak string 
        $pdf->Cell(190,7,'TOKO JAYA ABADI',0,1,'C');
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(190,7,'LAPORAN PEMBELIAN BARANG',0,1,'C');
        $pdf->SetFont('Arial','B',11);
        $pdf->Cell(190,7,$tgl_awal." s/d ".$tgl_akhir,0,1,'C');
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(25,6,'NO',1,0,'C');
        $pdf->Cell(40,6,'Nomer Transaksi',1,0,'C');
        $pdf->Cell(30,6,'Tanggal',1,0,'C');
        $pdf->Cell(30,6,'Total Barang',1,0,'C');
        $pdf->Cell(25,6,'Total Qty',1,0,'C');
        $pdf->Cell(40,6,'Jumlah Nominal',1,1,'C');
        $pdf->SetFont('Arial','',10);

        $no=0;
        $total=0;
        foreach ($report as $data) {
            $no++;

            $total_pembelian = "Rp " . number_format($data->total_pembelian,2,',','.');
            $pdf->Cell(25,6,$no,1,0,'C');
            $pdf->Cell(40,6,$data->no_transaksi,1,0,'C');
            $pdf->Cell(30,6,$data->tanggal,1,0,'C');
            $pdf->Cell(30,6,$data->total_barang,1,0,'C');
            $pdf->Cell(25,6,$data->total_qty,1,0,'C');
            $pdf->Cell(40,6,"Rp.".number_format($data->total_pembelian),1,1,'R');
            $total=+ $data->total_pembelian;
        }
        
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(150,6,'Total Keseluruhan',1,0,'R');
        $pdf->Cell(40,6,"Rp.".number_format($total),1,1,'R');

        $pdf->Output();
     
	}
    
}
