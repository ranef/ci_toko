<?php defined('BASEPATH') OR exit('No direct script access allowed');


class auth extends CI_Controller
{
	public function __construct()
	{
		//buat panggil scrip pertama kali dijalankan
		parent::__construct();
		//load model terkait
		$this->load->model("user_model");
	}

	public function index()
	{
		//cek login akses
		$user_login	=$this->session->userdata();

		if(count($user_login)<= 1){
		$this->login();
		}else {
		redirect("home/");
		}
	}
	

	public function login()
	{
		if(!empty($_REQUEST)){
		//ambil dari form login
		$username	=$this->input->post('username');
		$password	=$this->input->post('password');
		$data_login	=$this->user_model->cekUser($username, $password);
	

		//echo "<pre>";
		//print_r($data_login); die();
		//echo "</pre>";

		$data_sesi	= [
			'username'	=>$data_login['nik'],
			'email'		=>$data_login['email'],
			'tipe'		=>$data_login['tipe'],
			'status'	=>"login"
		];

		if(!empty($data_login)){

			//login berhasil
			$this->session->set_userdata($data_sesi);
			redirect("Home/", "refresh");
		}else {

			//login gagal
			$this->session->set_flashdata('info', 'Username atau Password Salah !');
			redirect("auth/", "refresh");
			}
		}

		$this->load->view('login');
	}

	public function daftar_user()
	{

		$this->session->sess_destroy();
		redirect("auth/", "refresh");

	}	

	public function register()
	{

		$this->load->view('v_register');

	}


	public function logout()
	{

		$this->session->sess_destroy();
		redirect("auth/", "refresh");

	}	
	
}