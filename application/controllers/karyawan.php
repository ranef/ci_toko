<?php defined('BASEPATH') OR exit('No direct script access allowed');


class karyawan extends CI_Controller
{
	public function __construct()
	{
		//buat panggil scrip pertama kali dijalankan
		parent::__construct();
		//load model terkait
		$this->load->model("karyawan_model");
		$this->load->model("jabatan_model");
		$this->load->library('form_validation');

		$user_login	=$this->session->userdata();

		if(count($user_login)<= 1){
		redirect("auth/index", "refresh");
		}
	}

	public function index()
	{
		$this->listKaryawan();
	}
	
	public function listKaryawan()
	{
		if(isset($_POST['tombol_cari'])){
			$data['kata_pencarian']=$this->input->post('cari');
			$this->session->set_userdata('session_pencarian_karyawan',$data['kata_pencarian']);
		}else{
			$data['kata_pencarian']=$this->session->userdata('session_pencarian_karyawan');
		}

		//$data['data_karyawan'] =$this->karyawan_model->tampilDataKaryawan();
		$data['data_karyawan']=$this->karyawan_model->tombolpagination($data['kata_pencarian']);
		$data['content']='forms/list_karyawan';
		$this->load->view('home',$data);
		
	}	
	
	public function inputKaryawan()
	{
		//$nik = $this->input->POST('nik');
		$data['data_jabatan']=$this->jabatan_model->tampilDataJabatan();
		$data['no_urut_nik']=$this->karyawan_model->createKodeUrut();
		$data['content']='forms/input_karyawan';
		$validation =$this->form_validation;
		$validation->set_rules($this->karyawan_model->rules_nik());

		// echo "<prev>";
		// print_r($this->jabatan_model->tampilDataJabatan());die();
		// echo "</prev>";
		if ($validation->run()){
			$this->karyawan_model->saveDataKaryawan();
			$this->session->set_flashdata('info', '<div style="color: green">Simpan Data Berhasil !</div>');
			redirect("karyawan/index", "refresh");
			}
		
		$this->load->view('home',$data);
	}
	
	public function detailKaryawan($nik)
	{
		$data['detailKaryawan']=$this->karyawan_model->detailKaryawan($nik);
		$data['content']='forms/detailKaryawan';
		$this->load->view('home',$data);
	}
	
	public function editKaryawan($nik)
	{
		$data['data_jabatan']=$this->jabatan_model->tampilDataJabatan();
		$data['content']='forms/editKaryawan';
		$data['editKaryawan']=$this->karyawan_model->editKaryawan($nik);
		$validation =$this->form_validation;
		$validation->set_rules($this->karyawan_model->rules());
		
		if ($validation->run()){
			$this->karyawan_model->updateKaryawan($nik);
			$this->session->set_flashdata('info', '<div style="color: green">Update Data Berhasil !</div>');
			redirect("karyawan/index", "refresh");
			}
		
		$this->load->view('home',$data);
	}

	public function deleteKaryawan($nik)
	{
		$data['content']='forms/list_karyawan';
		$m_karyawan=$this->karyawan_model;
		$m_karyawan->delete($nik);
		redirect("karyawan/index","refresh");
	}
}