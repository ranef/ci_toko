<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class penjualan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//load model terkait
		$this->load->model("penjualan_model");
        $this->load->model("barang_model");
        $this->load->library('form_validation');

        $user_login =$this->session->userdata();

        if(count($user_login)<= 1){
        redirect("auth/index", "refresh");
        }
	}

	public function index()
	{
        
		$this->listPenjualan();
    }
    
    public function listPenjualan()
	{
        
		if(isset($_POST['tombol_cari'])){
			$data['kata_pencarian']=$this->input->post('cari');
			$this->session->set_userdata('session_pencarian_penjualan',$data['kata_pencarian']);
		}else{
			$data['kata_pencarian']=$this->session->userdata('session_pencarian_penjualan');
		}

        //$data['data_karyawan'] =$this->karyawan_model->tampilDataKaryawan();
        $data['data_penjualan']=$this->penjualan_model->tombolpagination($data['kata_pencarian']);
		$data['content']='forms/list_penjualan';
		$this->load->view('home',$data);
    }
    
    public function input_h()
	{
        // panggil data supplier untuk kebutuhan form input
        $data['no_urut_transaksi']  =$this->penjualan_model->createKodeUrut();
        $data['content'] 		    = 'forms/input_penjualan';
        $validation =$this->form_validation;
		$validation->set_rules($this->penjualan_model->rules());
        if ($validation->run()){
        // proses simpan ke penjualan header jika ada request form
        if (!empty($_REQUEST)) {
            $penjualan_header = $this->penjualan_model;
            $penjualan_header->savepenjualanHeader();
            
            //panggil ID transaksi terakhir
            $id_terakhir = $penjualan_header->idTransaksiTerakhir();
            // var_dump($id_terakhir); die();
            
            // redirect("penjualan/index", "refresh");
            // redirect ke halaman input penjualan detail
			redirect("penjualan/input_d/" . $id_terakhir, "refresh");
        }}
        
		$this->load->view('home', $data);
    }
    
    public function input_d($id_jual_h)
	{
        // panggil data barang untuk kebutuhan form input   
        // $data['harga']=$this->barang_model->select();
        $data['data_barang'] 	= $this->barang_model->tampilDataBarang();
        $data['id_header'] 	    = $id_jual_h;
        $data['data_penjualan_detail'] 	= $this->penjualan_model->tampilDataPenjualanDetail($id_jual_h);
        $validation =$this->form_validation;
        $validation->set_rules($this->penjualan_model->rules1());
        
        if ($validation->run()){
        // proses simpan ke penjualan detail jika ada request form
        if (!empty($_REQUEST)) {
            //save detail
            $this->penjualan_model->savepenjualanDetail($id_jual_h);
            
            //proses update stok
            $qty        = $this->input->post('qty');
            $kode_barang = $this->input->POST('kode_barang');
            $this->barang_model->updateStokJual($kode_barang, $qty);
            $this->session->set_flashdata('info', '<div style="color: green">Simpan Data Berhasil !</div>');
            redirect("penjualan/input_d/" . $id_jual_h, "refresh");    
		}}
        
		$data['content'] 		= 'forms/input_penjualan_detail';
		$this->load->view('home', $data);
    }
     
}
