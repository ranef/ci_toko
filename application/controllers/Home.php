<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Home extends CI_Controller
{
	public function __construct()
	{
		//buat panggil scrip pertama kali dijalankan
		parent::__construct();

		$user_login	=$this->session->userdata();

		if(count($user_login)<= 1){
		redirect("auth/index", "refresh");
		}

	}

	public function index()
	{
		redirect("karyawan/index", "refresh");
		
	}	
	
}